﻿using ProkartApplication.ProkartWebServiceReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.Mvc;

namespace ProkartApplication.Controllers
{
    public class BuyController : Controller
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        ProkartWebServicesClient client = new ProkartWebServicesClient();
        // GET: Buy
        public ActionResult AllGadgets()
        {
            return View();
        }

        //POST
        [HttpPost]
        public string GetAllAvailableItems(int[] index)
        {
            try
            {
                return client.GetAllAvailableItems(index);
            }
            catch (FaultException<ProkartWebServiceReference.ProkartCustomException> e)
            {
                log.Error("Fault Exception handled     Time:" + e.Detail.Time + "      Method:" + e.Detail.ServiceName + Environment.NewLine + e.Detail.ExceptionMessage);
                return string.Empty;
            }
            catch (Exception e)
            {
                log.Error("Other Exception handled in GetAllAvailableItems() POST." + e.Message);
                return string.Empty;
            }
        }

        [HttpPost]
        public string GetSortedItems(string[] brands)
        {
            try
            {
                return client.GetSortedItems(brands);
            }
            catch (FaultException<ProkartWebServiceReference.ProkartCustomException> e)
            {
                log.Error("Fault Exception handled     Time:" + e.Detail.Time + "      Method:" + e.Detail.ServiceName + Environment.NewLine + e.Detail.ExceptionMessage);
                return string.Empty;
            }
            catch (Exception e)
            {
                log.Error("Other Exception handled in GetSortedItems() POST." + e.Message);
                return string.Empty;
            }
        }
    }
}