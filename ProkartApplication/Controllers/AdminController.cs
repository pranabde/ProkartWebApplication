﻿using ProkartApplication.ProkartWebServiceReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.Mvc;

namespace ProkartApplication.Controllers
{
    public class AdminController : Controller
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        ProkartWebServicesClient client = new ProkartWebServicesClient();
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Availability()
        {
            return View();
        }

        //POST
        [HttpPost]
        public ActionResult Index(string[] Tier1, string[] Tier2, string[] Tier3)
        {
            try
            {
                client.SetBrands(Tier1, Tier2, Tier3);
            }
            catch (FaultException<ProkartWebServiceReference.ProkartCustomException> e)
            {
                log.Error("Fault Exception handled     Time:" + e.Detail.Time + "      Method:" + e.Detail.ServiceName + Environment.NewLine + e.Detail.ExceptionMessage);
            }
            catch (Exception e)
            {
                log.Error("Other Exception handled in Index() POST." + e.Message);
            }
            return View();
        }

        [HttpPost]
        public void UpdateGadgetStatus(long[] orderIds, string status)
        {
            try
            {
                client.UpdateGadgetStatus(orderIds, status);
            }
            catch (FaultException<ProkartWebServiceReference.ProkartCustomException> e)
            {
                log.Error("Fault Exception handled     Time:" + e.Detail.Time + "      Method:" + e.Detail.ServiceName + Environment.NewLine + e.Detail.ExceptionMessage);
            }
            catch (Exception e)
            {
                log.Error("Other Exception handled in UpdateGadgetStatus() POST." + e.Message);
            }
        }

        [HttpPost]
        public void UpdateGadgetDetails(Device[] data)
        {
            try
            {
                client.UpdateGadgetDetails(data);
            }
            catch (FaultException<ProkartWebServiceReference.ProkartCustomException> e)
            {
                log.Error("Fault Exception handled     Time:" + e.Detail.Time + "      Method:" + e.Detail.ServiceName + Environment.NewLine + e.Detail.ExceptionMessage);
            }
            catch (Exception e)
            {
                log.Error("Other Exception handled in UpdateGadgetDetails() POST." + e.Message);
            }
        }
    }
}