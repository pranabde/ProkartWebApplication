﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using ProkartApplication.ProkartWebServiceReference;
using System.ServiceModel;

namespace ProkartApplication.Controllers
{
    public class SellController : Controller
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        ProkartWebServicesClient client = new ProkartWebServicesClient();
        // GET: Sell
        public ActionResult Search()
        {
            return View();
        }
        public ActionResult Specification()
        {
            return View();
        }
        
        public ActionResult Questionnaire()
        {
            return View();
        }

        public ActionResult PriceEstimate()
        {
            return View();
        }

        public ActionResult OtherDevices()
        {
            return View();
        }

        public ActionResult CheckOut()
        {
            return View();
        }

        public ActionResult Greetings()
        {
            return View();
        }

        //POST: Sell
        [HttpPost]
        public ActionResult Search(Device data)
        {
            try
            {
                string json = string.Empty;
                json = client.SetDeviceDetails(data);
                Session["Search"] = json;
                return RedirectToAction("Specification");
            }
            catch (FaultException<ProkartWebServiceReference.ProkartCustomException> e)
            {
                log.Error("Fault Exception handled     Time:"+e.Detail.Time+"      Method:"+e.Detail.ServiceName+Environment.NewLine+e.Detail.ExceptionMessage);
                Session["Search"] = string.Empty;
                return RedirectToAction("Specification");
            }
            catch (Exception e)
            {
                log.Error("Other Exception handled in Search() POST." + e.Message);
                Session["Search"] = string.Empty;
                return RedirectToAction("Specification");
            }
        }

        [HttpPost]
        public ActionResult Specification(Device data)
        {
            try
            {
                string json = string.Empty;
                json = client.SetDeviceSpecification(data);
                Session["Specification"] = json;
                return RedirectToAction("Questionnaire");
            }
            catch (FaultException<ProkartWebServiceReference.ProkartCustomException> e)
            {
                log.Error("Fault Exception handled     Time:" + e.Detail.Time + "      Method:" + e.Detail.ServiceName + Environment.NewLine + e.Detail.ExceptionMessage);
                return RedirectToAction("Search");
            }
            catch (Exception e)
            {
                log.Error("Other Exception handled in Specification() POST." + e.Message);
                return RedirectToAction("Search");
            }
        }

        [HttpPost]
        public ActionResult Questionnaire(Device data)
        {
            try
            {
                string json = string.Empty;
                Session["prokart_session"] = Convert.ToString(data.SessionId);
                json = client.GetDevicePrice(data);
                Session["Questionnaire"] = json;
                return RedirectToAction("PriceEstimate");
            }
            catch (FaultException<ProkartWebServiceReference.ProkartCustomException> e)
            {
                log.Error("Fault Exception handled     Time:" + e.Detail.Time + "      Method:" + e.Detail.ServiceName + Environment.NewLine + e.Detail.ExceptionMessage);
                return RedirectToAction("Search");
            }
            catch (Exception e)
            {
                log.Error("Other Exception handled in Questionnaire() POST." + e.Message);
                return RedirectToAction("Search");
            }
        }

        [HttpPost]
        public ActionResult PriceEstimate(Device data)
        {
            try
            {
                string json = string.Empty;
                json = client.SetDeviceDetailsAfter(data);
                Session["PriceEstimate"] = json;
                return RedirectToAction("CheckOut");
            }
            catch (FaultException<ProkartWebServiceReference.ProkartCustomException> e)
            {
                log.Error("Fault Exception handled     Time:" + e.Detail.Time + "      Method:" + e.Detail.ServiceName + Environment.NewLine + e.Detail.ExceptionMessage);
                return RedirectToAction("Search");
            }
            catch (Exception e)
            {
                log.Error("Other Exception handled in PriceEstimate() POST." + e.Message);
                return RedirectToAction("Search");
            }
        }

        [HttpPost]
        public ActionResult CheckOut(Device data)
        {
            string json = string.Empty;
            try
            {
                data.SessionId = Convert.ToString(Session["prokart_session"]);
                json = client.CheckOut(data);
                Session["CheckOut"] = json;
                return RedirectToAction("Greetings");
            }
            catch (FaultException<ProkartWebServiceReference.ProkartCustomException> e)
            {
                log.Error("Fault Exception handled     Time:" + e.Detail.Time + "      Method:" + e.Detail.ServiceName + Environment.NewLine + e.Detail.ExceptionMessage);
                var reply = new
                {
                    FirstName = data.FirstName,
                    Message = e.Message
                };
                Session["CheckOut"] = reply;
                return RedirectToAction("Greetings");
            }
            catch (Exception e)
            {
                log.Error("Other Exception handled in CheckOut() POST." + e.Message);
                return RedirectToAction("Search");
            }
        }

        [HttpPost]
        public ActionResult OtherDevices(Device data)
        {
            try
            {
                string json = string.Empty;
                json = client.SetDeviceDetailsForOtherDevices(data);
                Session["Specification"] = json;
                return RedirectToAction("Questionnaire");
            }
            catch (FaultException<ProkartWebServiceReference.ProkartCustomException> e)
            {
                log.Error("Fault Exception handled     Time:" + e.Detail.Time + "      Method:" + e.Detail.ServiceName + Environment.NewLine + e.Detail.ExceptionMessage);
                return RedirectToAction("Search");
            }
            catch (Exception e)
            {
                log.Error("Other Exception handled in OtherDevices() POST." + e.Message);
                return RedirectToAction("Search");
            }
        }
    }
}