﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ProkartApplication
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "Admin",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Admin", action = "Admin/Availability", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "Brands",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Admin", action = "Admin/Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "Buy",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Buy", action = "Buy/AllGadgets", id = UrlParameter.Optional }
            );
        }
    }
}
