//======================================================================================
//************** get_records.js gets/populates/updates records from/to database ********
//                                                                              ********
//======================================================================================
//****************************** Author: Pranab Kumar De *******************************
//======================================================================================

$(document).ready(function () {
    //Initializing local variables
    var result;
    var selectedOrderIds = [];
    var selectedBrands = [];
    var selectedModels = [];
    var selectedCategory = [];
    var selectedRam = [];
    var selectedMemory = [];
    var selectedCamera = [];
    var selectedBat_Con = [];
    var selectedScrn_Qlty = [];
    var selectedCP_Con = [];
    var selectedCam_Con = [];
    var selectedH_Prob = [];
    var selectedS_Prob = [];
    var selectedPrice = [];

    //******************************************************************
    //Ajax call and table creation for filling up the data from database.
    //Modified 8/10/2016
    //*******************************************************************
    $.ajax({
        url: 'http://prokartwebservice.us-west-2.elasticbeanstalk.com/ProkartWebServices.svc/WebRequest/GetGadgetData',
        type: 'POST',
        async: false,
        datatype: "application/json",
        statusCode: {
        },
        success: function (reply) {
            result = JSON.parse(reply);
            var tr;
            for (var i = 0; i < result.length; i++) {
                tr = $('<tr id="tr'+i+'">');
                tr.append('<td><input type=' + '"checkbox" ' + 'id="' + i + '"' + '/>' + '</td>');
                tr.append("<td>" + result[i].order_id + "</td>");
                tr.append("<td>" + result[i].created_time + "</td>");
                tr.append("<td>" + result[i].modified_time + "</td>");
                tr.append("<td>" + result[i].brand + "</td>");
                tr.append("<td>" + result[i].model + "</td>");
                tr.append("<td>" + result[i].release_date + "</td>");
                tr.append("<td>" + result[i].ram + "</td>");
                tr.append("<td>" + result[i].memory + "</td>");
                tr.append("<td>" + result[i].camera + "</td>");
                tr.append("<td>" + result[i].price + "</td>");
                tr.append("<td>" + result[i].availability + "</td>");
                tr.append('</tr>');
                $('#list').append(tr);
            }
        },
        error: function (error, status, msg) {
            alert("Error while fetching mobile brands");
        }
    });

    //***********************************************************************
    //** On clicking 'Processing' button, all records with 'processing' status 
    //** will be populated.
    //***********************************************************************
    $('#processing').on('click', function () {
        var processingRecords = [];
        for (var count = 0; count < result.length; count++) {
            if (result[count].availability == "processing") {
                processingRecords.push(result[count]);
            }
        }
        $('#list tbody').empty();
        var tr;
        for (var i = 0; i < processingRecords.length; i++) {
            tr = $('<tr id="tr' + i + '">');
            tr.append('<td><input type=' + '"checkbox" ' + 'id="' + i + '"' + '/>' + '</td>');
            tr.append("<td>" + processingRecords[i].order_id + "</td>");
            tr.append("<td>" + processingRecords[i].created_time + "</td>");
            tr.append("<td>" + processingRecords[i].modified_time + "</td>");
            tr.append("<td>" + processingRecords[i].brand + "</td>");
            tr.append("<td>" + processingRecords[i].model + "</td>");
            tr.append("<td>" + processingRecords[i].release_date + "</td>");
            tr.append("<td>" + processingRecords[i].ram + "</td>");
            tr.append("<td>" + processingRecords[i].memory + "</td>");
            tr.append("<td>" + processingRecords[i].camera + "</td>");
            tr.append("<td>" + processingRecords[i].price + "</td>");
            tr.append("<td>" + processingRecords[i].availability + "</td>");
            tr.append('</tr>');
            $('#list').append(tr);
        }
        pagination();
    });

    //***********************************************************************
    //** On clicking 'Available' button, all records with 'available' status 
    //** will be populated.
    //***********************************************************************
    $('#available').on('click', function () {
        var availableRecords = [];
        for (var count = 0; count < result.length; count++) {
            if (result[count].availability == "available") {
                availableRecords.push(result[count]);
            }
        }
        $('#list tbody').empty();
        var tr;
        for (var i = 0; i < availableRecords.length; i++) {
            tr = $('<tr id="tr' + i + '">');
            tr.append('<td><input type=' + '"checkbox" ' + 'id="' + i + '"' + '/>' + '</td>');
            tr.append("<td>" + availableRecords[i].order_id + "</td>");
            tr.append("<td>" + availableRecords[i].created_time + "</td>");
            tr.append("<td>" + availableRecords[i].modified_time + "</td>");
            tr.append("<td>" + availableRecords[i].brand + "</td>");
            tr.append("<td>" + availableRecords[i].model + "</td>");
            tr.append("<td>" + availableRecords[i].release_date + "</td>");
            tr.append("<td>" + availableRecords[i].ram + "</td>");
            tr.append("<td>" + availableRecords[i].memory + "</td>");
            tr.append("<td>" + availableRecords[i].camera + "</td>");
            tr.append("<td>" + availableRecords[i].price + "</td>");
            tr.append("<td>" + availableRecords[i].availability + "</td>");
            tr.append('</tr>');
            $('#list').append(tr);
        }
        pagination();
    });

    //***********************************************************************
    //** On clicking 'Sold' button, all records with 'sold' status 
    //** will be populated.
    //***********************************************************************
    $('#sold').on('click', function () {
        var soldRecords = [];
        for (var count = 0; count < result.length; count++) {
            if (result[count].availability == "sold") {
                soldRecords.push(result[count]);
            }
        }
        $('#list tbody').empty();
        var tr;
        for (var i = 0; i < soldRecords.length; i++) {
            tr = $('<tr id="tr' + i + '">');
            tr.append('<td><input type=' + '"checkbox" ' + 'id="' + i + '"' + '/>' + '</td>');
            tr.append("<td>" + soldRecords[i].order_id + "</td>");
            tr.append("<td>" + soldRecords[i].created_time + "</td>");
            tr.append("<td>" + soldRecords[i].modified_time + "</td>");
            tr.append("<td>" + soldRecords[i].brand + "</td>");
            tr.append("<td>" + soldRecords[i].model + "</td>");
            tr.append("<td>" + soldRecords[i].release_date + "</td>");
            tr.append("<td>" + soldRecords[i].ram + "</td>");
            tr.append("<td>" + soldRecords[i].memory + "</td>");
            tr.append("<td>" + soldRecords[i].camera + "</td>");
            tr.append("<td>" + soldRecords[i].price + "</td>");
            tr.append("<td>" + soldRecords[i].availability + "</td>");
            tr.append('</tr>');
            $('#list').append(tr);
        }
        pagination();
    });

    //***********************************************************************
    //** On clicking 'Cancelled' button, all records with 'cancelled' status 
    //** will be populated.
    //***********************************************************************
    $('#cancelled').on('click', function () {
        var cancelledRecords = [];
        for (var count = 0; count < result.length; count++) {
            if (result[count].availability == "cancelled") {
                cancelledRecords.push(result[count]);
            }
        }
        $('#list tbody').empty();
        var tr;
        for (var i = 0; i < cancelledRecords.length; i++) {
            tr = $('<tr id="tr' + i + '">');
            tr.append('<td><input type=' + '"checkbox" ' + 'id="' + i + '"' + '/>' + '</td>');
            tr.append("<td>" + cancelledRecords[i].order_id + "</td>");
            tr.append("<td>" + cancelledRecords[i].created_time + "</td>");
            tr.append("<td>" + cancelledRecords[i].modified_time + "</td>");
            tr.append("<td>" + cancelledRecords[i].brand + "</td>");
            tr.append("<td>" + cancelledRecords[i].model + "</td>");
            tr.append("<td>" + cancelledRecords[i].release_date + "</td>");
            tr.append("<td>" + cancelledRecords[i].ram + "</td>");
            tr.append("<td>" + cancelledRecords[i].memory + "</td>");
            tr.append("<td>" + cancelledRecords[i].camera + "</td>");
            tr.append("<td>" + cancelledRecords[i].price + "</td>");
            tr.append("<td>" + cancelledRecords[i].availability + "</td>");
            tr.append('</tr>');
            $('#list').append(tr);
        }
        pagination();
    });

    //***********************************************************************
    //** On clicking 'All' button, all records will be populated.
    //***********************************************************************
    $('#all').on('click', function () {
        var allRecords = result;
        $('#list tbody').empty();
        var tr;
        for (var i = 0; i < allRecords.length; i++) {
            tr = $('<tr id="tr' + i + '">');
            tr.append('<td><input type=' + '"checkbox" ' + 'id="' + i + '"' + '/>' + '</td>');
            tr.append("<td>" + allRecords[i].order_id + "</td>");
            tr.append("<td>" + allRecords[i].created_time + "</td>");
            tr.append("<td>" + allRecords[i].modified_time + "</td>");
            tr.append("<td>" + allRecords[i].brand + "</td>");
            tr.append("<td>" + allRecords[i].model + "</td>");
            tr.append("<td>" + allRecords[i].release_date + "</td>");
            tr.append("<td>" + allRecords[i].ram + "</td>");
            tr.append("<td>" + allRecords[i].memory + "</td>");
            tr.append("<td>" + allRecords[i].camera + "</td>");
            tr.append("<td>" + allRecords[i].price + "</td>");
            tr.append("<td>" + allRecords[i].availability + "</td>");
            tr.append('</tr>');
            $('#list').append(tr);
        }
        pagination();
    });

    //*************************************************************************
    //**** Description: A search box which filters records from the input given 
    //**** It searches for match from the order id and populates in table. ****
    //*************************************************************************
    $('#order-search').on('input', function () {
        var searchText = $('#order-search').val();
        var length = searchText.length;
        var sortedByOrderId = [];
        for (var j = 0; j < result.length; j++) {
            if (searchText.toString() === result[j].order_id.toString().substring(0, length)) {
                sortedByOrderId.push(result[j]);
            }
        }
        $('#list tbody').empty();
        var tr;
        for (var i = 0; i < sortedByOrderId.length; i++) {
            tr = $('<tr id="tr' + i + '">');
            tr.append('<td><input type=' + '"checkbox" ' + 'id="' + i + '"' + '/>' + '</td>');
            tr.append("<td>" + sortedByOrderId[i].order_id + "</td>");
            tr.append("<td>" + sortedByOrderId[i].created_time + "</td>");
            tr.append("<td>" + sortedByOrderId[i].modified_time + "</td>");
            tr.append("<td>" + sortedByOrderId[i].brand + "</td>");
            tr.append("<td>" + sortedByOrderId[i].model + "</td>");
            tr.append("<td>" + sortedByOrderId[i].release_date + "</td>");
            tr.append("<td>" + sortedByOrderId[i].ram + "</td>");
            tr.append("<td>" + sortedByOrderId[i].memory + "</td>");
            tr.append("<td>" + sortedByOrderId[i].camera + "</td>");
            tr.append("<td>" + sortedByOrderId[i].price + "</td>");
            tr.append("<td>" + sortedByOrderId[i].availability + "</td>");
            tr.append('</tr>');
            $('#list').append(tr);
        }
        pagination();
    });
    pagination();

    //****************** Function pagination() creates page view for all the records 
    //****************** Per page record count is 10
    //******************************************************************************
    function pagination() {
        $('#nav').remove();
        $('#list').after('<div id="nav" style="float:left"></div>');
        var rowsShown = 10;
        var rowsTotal = $('#list tbody tr').length;
        var numPages = rowsTotal / rowsShown;
        for (i = 0; i < numPages; i++) {
            var pageNum = i + 1;
            $('#nav').append('<a href="#" rel="' + i + '">' + pageNum + '</a> ');
        }
        $('#list tbody tr').hide();
        $('#list tbody tr').slice(0, rowsShown).show();
        $('#nav a:first').addClass('active');
        $('#nav a').bind('click', function () {

            $('#nav a').removeClass('active');
            $(this).addClass('active');
            var currPage = $(this).attr('rel');
            var startItem = currPage * rowsShown;
            var endItem = startItem + rowsShown;
            $('#list tbody tr').css('opacity', '0.0').hide().slice(startItem, endItem).
                    css('display', 'table-row').animate({ opacity: 1 }, 300);
        });
    };

    //********* Clicking 'Change Status' button changes the status of the selected records ***
    //********* to the immediate next level. ('processing'->'available' & 'available'->'sold')
    //****************************************************************************************
    $('#change').on('click', function () {
        var selectedOrderIds = [];
        var selectedCategory = [];
        var status = '';
        var table = document.getElementById('list');
        for (var r = 1, n = table.rows.length; r < n; r++) {
            var id = document.getElementById(r - 1);
            if ($(id).prop("checked") == true) {
                selectedOrderIds.push(table.rows[r].cells[1].innerHTML);
                selectedCategory.push(table.rows[r].cells[11].innerHTML);
            }
        }
        for (var i = 0; i < selectedCategory.length; i++) {
            for (var j = i + 1; j < selectedCategory.length; j++) {
                if (selectedCategory[i] == selectedCategory[j]) {
                    continue;
                }
                else {
                    alert("Selection should be of similar category.");
                    selectedCategory = [];
                    break;
                }
            }
        }
        if (selectedCategory.length > 0) {
            if (selectedCategory[0] == 'processing') {
                status = 'available';
            }
            if (selectedCategory[0] == 'available') {
                status = 'sold';
            }
            if (selectedCategory[0] == 'sold') {
                alert('Selected category status is already "sold"');
                status = '';
            }
            if (status != '' && selectedOrderIds.length > 0) {
                var data = {
                    orderIds: selectedOrderIds,
                    status: status
                };
                $.ajax({
                    url: 'http://prokart.us-west-2.elasticbeanstalk.com/Admin/UpdateGadgetStatus',
                    data: data,
                    type: 'POST',
                    async: false,
                    contenttype: "application/json",
                    datatype: "json",
                    success: function (result) {
                        alert("Success");
                        window.location.href = 'http://prokart.us-west-2.elasticbeanstalk.com/Admin/Availability';
                    },
                    error: function (error, status, msg) {
                        alert("Error Updating Records");
                    }
                });
            }
        }
    });

    //********* Clicking 'Cancel Order' button changes the status of the selected records
    //********* to 'cancelled'. This process is irreversible ****************************
    //***********************************************************************************
    $('#cancel').on('click', function () {
        var selectedOrderIds = [];
        var selectedCategory = [];
        var status = '';
        var table = document.getElementById('list');
        for (var r = 1, n = table.rows.length; r < n; r++) {
            var id = document.getElementById(r - 1);
            if ($(id).prop("checked") == true) {
                selectedOrderIds.push(table.rows[r].cells[1].innerHTML);
                selectedCategory.push(table.rows[r].cells[11].innerHTML);
            }
        }
        for (var i = 0; i < selectedCategory.length; i++) {
            if (selectedCategory[i] == 'processing') {
                continue;
            }
            else {
                alert("Selection should be in 'Processing' stage.");
                selectedCategory = [];
                selectedOrderIds = [];
                break;
            }
        }
        if (selectedCategory.length > 0 && selectedOrderIds.length > 0) {
            status = 'cancelled';
            var data = {
                orderIds: selectedOrderIds,
                status: status
            };
            $.ajax({
                url: 'http://prokart.us-west-2.elasticbeanstalk.com/Admin/UpdateGadgetStatus',
                data: data,
                type: 'POST',
                async: false,
                contenttype: "application/json",
                datatype: "json",
                success: function (result) {
                    alert("Order Cancelled");
                    window.location.href = 'http://prokart.us-west-2.elasticbeanstalk.com/Admin/Availability';
                },
                error: function (error, status, msg) {
                    alert("Error Updating Records");
                }
            });
        }
    });

    //********* Clicking the check boxes will change the color of the 
    //********* corresponding table row element's color to red. 
    //********* Unchecking will change the color back to default.
    //***************************************************************
    $("table").on("change", ":checkbox", function () {
        var table = document.getElementById('list');
        for (var r = 0, n = table.rows.length; r < n; r++) {
            var id = document.getElementById(r);
            var tr = document.getElementById('tr' + r);
            if ($(id).prop("checked") == true) {                
                $(tr).css('color', '#ff0000');
            }
            if ($(id).prop("checked") == false) {
                $(tr).css('color', '');
            }
        }
    });

    //********** Clicking 'Edit Order' button will open a new modal popup table ****
    //********** with selected orders. Admin can modify the device specifications **
    //********** as per the input given by the inspector and update the database ***
    //********** accordingly. ******************************************************
    //******************************************************************************
    $('#edit').on('click', function () {
        $('#update-records').css('overflow', 'hidden');
        selectedOrderIds = [];
        selectedBrands = [];
        selectedModels = [];
        selectedCategory = [];
        selectedRam = [];
        selectedMemory = [];
        selectedPrice = [];
        selectedBat_Con = [];
        selectedScrn_Qlty = [];
        selectedCP_Con = [];
        selectedCam_Con = [];
        selectedH_Prob = [];
        selectedS_Prob = [];
        var flag = true;
        var status = '';
        var table = document.getElementById('list');
        for (var r = 1, n = table.rows.length; r < n; r++) {
            var id = document.getElementById(r - 1);
            if ($(id).prop("checked") == true) {
                selectedOrderIds.push(table.rows[r].cells[1].innerHTML);
                selectedBrands.push(table.rows[r].cells[4].innerHTML);
                selectedModels.push(table.rows[r].cells[5].innerHTML);
                selectedCategory.push(table.rows[r].cells[11].innerHTML);
                selectedRam.push(table.rows[r].cells[7].innerHTML);
                selectedMemory.push(table.rows[r].cells[8].innerHTML);
                selectedCamera.push(table.rows[r].cells[9].innerHTML);
                selectedPrice.push(table.rows[r].cells[10].innerHTML);
            }
        }
        if (selectedOrderIds.length == 0) {
            alert("No records selected");
            flag = false;
        }
        if (selectedOrderIds.length > 0) {
            for (var i = 0; i < selectedCategory.length; i++) {
                if (selectedCategory[i] == 'sold' || selectedCategory[i] == 'cancelled') {
                    selectedCategory = [];
                    selectedOrderIds = [];
                    alert("Selection contains Sold/Cancelled items which can not be edited.");
                    flag = false;
                    break;
                }
                else {
                    continue;
                }
            }
        }
        if (flag == true) {
            $('#sorted-list tbody').empty();
            for (var i = 0; i < selectedOrderIds.length; i++) {
                for (var j = 0; j < result.length; j++) {
                    if (result[j].order_id == selectedOrderIds[i]) {
                        selectedBat_Con.push(result[j].battery_condition);
                        selectedScrn_Qlty.push(result[j].screen_quality);
                        selectedCP_Con.push(result[j].charging_port_condition);
                        selectedCam_Con.push(result[j].camera_condition);
                        selectedH_Prob.push(result[j].heating_problem);
                        selectedS_Prob.push(result[j].software_problem);
                    }
                }
            }
            //Creating table with editable fields.
            //************************************
            $('#sorted-list tbody').empty();
            var tr;
            for (var i = 0; i < selectedOrderIds.length; i++) {
                tr = $('<tr id="s-tr' + i + '">');
                tr.append('<td>' + selectedOrderIds[i] + '</td>');
                tr.append('<td><input type=' + '"text" ' + 'value="' + selectedBrands[i] + '"' + 'style="width: 100px" id="brand' + i + '"/>' + '</td>');
                tr.append('<td><input type=' + '"text" ' + 'value="' + selectedModels[i] + '"' + 'style="width: 145px" id="model' + i + '"/>' + '</td>');
                tr.append('<td><input type=' + '"text" ' + 'value="' + selectedRam[i] + '"' + 'style="width: 65px" id="ram'+i+'"/>' + '</td>');
                tr.append('<td><input type=' + '"text" ' + 'value="' + selectedMemory[i] + '"' + 'style="width: 65px" id="memory'+i+'"/>' + '</td>');
                tr.append('<td><input type=' + '"text" ' + 'value="' + selectedCamera[i] + '"' + 'style="width: 70px" id="camera'+i+'"/>' + '</td>');
                tr.append('<td><select id="batcon'+i+'"><option disabled selected>' + selectedBat_Con[i] + '</option><option>1</option><option>2</option><option>3</option><option>4</option><option>5</option><option>6</option><option>7</option><option>8</option><option>9</option><option>10</option></select></td>');
                tr.append('<td><select id="scrnqlty'+i+'"><option disabled selected>' + selectedScrn_Qlty[i] + '</option><option>1</option><option>2</option><option>3</option><option>4</option><option>5</option><option>6</option><option>7</option><option>8</option><option>9</option><option>10</option></select></td>');
                tr.append('<td><select id="cpcon'+i+'"><option disabled selected>' + selectedCP_Con[i] + '</option><option>1</option><option>2</option><option>3</option><option>4</option><option>5</option><option>6</option><option>7</option><option>8</option><option>9</option><option>10</option></select></td>');
                tr.append('<td><select id="camcon'+i+'"><option disabled selected>' + selectedCam_Con[i] + '</option><option>1</option><option>2</option><option>3</option><option>4</option><option>5</option><option>6</option><option>7</option><option>8</option><option>9</option><option>10</option></select></td>');
                tr.append('<td><select id="hprob'+i+'"><option disabled selected>' + selectedH_Prob[i] + '</option><option>1</option><option>2</option><option>3</option><option>4</option><option>5</option><option>6</option><option>7</option><option>8</option><option>9</option><option>10</option></select></td>');
                tr.append('<td><select id="sprob'+i+'"><option disabled selected>' + selectedS_Prob[i] + '</option><option>1</option><option>2</option><option>3</option><option>4</option><option>5</option><option>6</option><option>7</option><option>8</option><option>9</option><option>10</option></select></td>');
                tr.append('<td><input type=' + '"text" ' + 'value="' + selectedPrice[i] + '"' + 'style="width: 80px" id="price'+i+'"/>' + '</td>');
                tr.append('</tr>');
                $('#sorted-list').append(tr);
            }
            //********************************************
            //Opening the modal popup window.
            //********************************************
            $("#update-records").show();
            if (selectedOrderIds.length > 10) {
                $('#update-records').css('overflow', 'scroll');
            }
            var modal = document.getElementById('update-records');
            var span = document.getElementsByClassName("availabilityclose")[0];
            modal.style.display = "block";

            //Closing the modal popup window when clicked (X) or outside the division.
            //************************************************************************
            span.onclick = function () {
                $('#sorted-list tbody').empty();
                modal.style.display = "none";
            }
            window.onclick = function (event) {
                if (event.target == modal) {
                    $('#sorted-list tbody').empty();
                    modal.style.display = "none";
                }
            }            
        }
    });

    //********* Clicking on 'Submit' button will update the database
    //**************************************************************
    $('#submit').on('click', function () {
        var number_regex = /^[0-9]+$/;
        var sortedTable = document.getElementById('sorted-list');
        selectedBrands = [];
        selectedModels = [];
        selectedRam = [];
        selectedMemory = [];
        selectedCamera = [];
        selectedPrice = [];
        selectedBat_Con = [];
        selectedScrn_Qlty = [];
        selectedCP_Con = [];
        selectedCam_Con = [];
        selectedH_Prob = [];
        selectedS_Prob = [];
        var flag = true;
        for (var i = 0; i < sortedTable.rows.length - 1; i++) {
            selectedBrands.push(document.getElementById('brand' + i).value);
            selectedModels.push(document.getElementById('model' + i).value);
            selectedRam.push(document.getElementById('ram' + i).value);
            selectedMemory.push(document.getElementById('memory' + i).value);
            selectedCamera.push(document.getElementById('camera' + i).value);
            selectedPrice.push(document.getElementById('price' + i).value);
            selectedBat_Con.push(document.getElementById('batcon' + i).value);
            selectedScrn_Qlty.push(document.getElementById('scrnqlty' + i).value);
            selectedCP_Con.push(document.getElementById('cpcon' + i).value);
            selectedCam_Con.push(document.getElementById('camcon' + i).value);
            selectedH_Prob.push(document.getElementById('hprob' + i).value);
            selectedS_Prob.push(document.getElementById('sprob' + i).value);
        }
        for (var j = 0; j < selectedRam.length; j++) {
            var ramStrings = selectedRam[j].split(" ");
            if (!ramStrings[0].match(number_regex) || (ramStrings[1] != 'GB' && ramStrings[1] != 'MB' && ramStrings[1] != 'gb' && ramStrings[1] != 'mb' && ramStrings[1] != 'Gb' && ramStrings[1] != 'Mb')) {
                alert(selectedOrderIds[j] + " has invalid 'RAM' input");
                flag = false;
            }
        }
        for (var k = 0; k < selectedMemory.length; k++) {
            var memoryStrings = selectedMemory[k].split(" ");
            if (!memoryStrings[0].match(number_regex) || (memoryStrings[1] != 'GB' && memoryStrings[1] != 'MB' && memoryStrings[1] != 'gb' && memoryStrings[1] != 'mb' && memoryStrings[1] != 'Gb' && memoryStrings[1] != 'Mb')) {
                alert(selectedOrderIds[k] + " has invalid 'Memory' input");
                flag = false;
            }
        }
        for (var l = 0; l < selectedPrice.length; l++) {
            if (!selectedPrice[l].match(number_regex)) {
                alert(selectedOrderIds[l] + " has invalid 'Price' input");
                flag = false;
            }
        }
        if (flag == true) {
            var dataArray = [];
            for (var count = 0; count < selectedOrderIds.length; count++) {
                var data = {
                    orderId:                selectedOrderIds[count],
                    brand:                  selectedBrands[count],
                    model:                  selectedModels[count],
                    ram:                    selectedRam[count],
                    memory:                 selectedMemory[count],
                    camera:                 selectedCamera[count],
                    batteryCondition:       selectedBat_Con[count],
                    screenQuality:          selectedScrn_Qlty[count],
                    chargingPortCondition:  selectedCP_Con[count],
                    cameraCondition:        selectedCam_Con[count],
                    heatingProblem:         selectedH_Prob[count],
                    softwareProblem:        selectedS_Prob[count],
                    price:                  selectedPrice[count]
                };
                dataArray.push(data);
                var jsonData = JSON.stringify(dataArray);
            }
            $.ajax({
                url: 'http://prokart.us-west-2.elasticbeanstalk.com/Admin/UpdateGadgetDetails',
                data: jsonData,
                type: 'POST',
                async: false,
                contentType: "application/json",
                datatype: "json",
                success: function (result) {
                    alert("Records updated.");
                    window.location.href = 'http://prokart.us-west-2.elasticbeanstalk.com/Admin/Availability';
                },
                error: function (error, status, msg) {
                    alert("Error Updating Records");
                }
            });
        }
    });
})