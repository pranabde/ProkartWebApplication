$( document ).ready(function() {

	$('.tab a').on('click', function (e) {

		e.preventDefault();

		$(this).parent().addClass('active');
		$(this).parent().siblings().removeClass('active');

		target = $(this).attr('href');

		$('.tab-content > div').not(target).hide();

		$(target).fadeIn(600);

	});
	
	var email = false,otp=false;
	$("#forgot_prokart_email").change(function(e){
		email = false,otp=false;
		$('#forgot_prokart_otp').val('');
		$("#prokart_otp").hide();
		var email_value = $("#forgot_prokart_email").val();
		var EMAIL_REGEX = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		e.preventDefault();
		if(EMAIL_REGEX.test(email_value)){
			$.ajax({
				url : 'http://192.168.0.100:7080/prokart/emailalreadyexists?email='+email_value,
				contentType : "application/json",
				type : "POST",
				success : function(response,status) {
					if(response.success_type == 'success'){
						notify('error','Account email does not exist','/Resource/image/notification_error.png','2000','sound2');
						$("#prokart_otp").hide();
					}
					if(response.error_type == 'error'){
						notify('success','Email id is correct','/Resource/image/notification_success.png','2000','sound1');
						email = true;
						$.ajax({
							url : 'http://192.168.0.100:7080/prokart/sendotp?email='+email_value,
							contentType : "application/json",
							type : "POST",
							success : function(response,status) {
								if(response.success_type == 'otp_success'){
									notify('success','Please enter the OTP send to your mobile number/Email address','/Resource/image/notification_success.png','3000','sound1');
									$("#prokart_otp").show();
									$("#prokart_otp_response").val(response.success_description.split("|")[0]);
									$("#prokart_otp_create_time").val(response.success_description.split("|")[1]);
								}
							},
							error : function(errorresponse) {
								notify('error','Please try after sometime','/Resource/image/notification_error.png','2000','sound2');
							}
						});   
					}
				},
				error : function(errorresponse) {
					notify('error','Please try after sometime','/Resource/image/notification_error.png','2000','sound2');
				}
			});
		}else{
			notify('error','Email is invalid!!!','/Resource/image/notification_error.png','2000','sound2');
		}
	});

	$("#forgot_prokart_password").on('change',function (e) {
		var new_password = $("#forgot_prokart_password").val();
		var cnf_new_pwd = $("#forgot_prokart_cnfpassword").val();
		var pwdLength = new_password.length;
		e.preventDefault();
		if(new_password){
			if(pwdLength < 5 || pwdLength >= 50){
				notify('error','New Password should be between 5 and 50 characters','/Resource/image/notification_error.png','2000','sound2');
			}/*else if(new_password == cnf_new_pwd){
			}else{
				notify('error','New Password  and Confirm New Password should match','/Resource/image/notification_error.png','2000','sound2');
			}*/
		}else{
			notify('error','New Password is required','/Resource/image/notification_error.png','2000','sound2');
		}
	});

	$("#forgot_prokart_cnfpassword").on('change',function (e) {
		var cnf_new_pwd = $("#forgot_prokart_cnfpassword").val();
		var new_password = $("#forgot_prokart_password").val();
		var pwdLength = cnf_new_pwd.length;
		e.preventDefault();
		if(cnf_new_pwd){
			if(pwdLength < 5 || pwdLength >= 50){
				notify('error','Confirm New Password should be between 5 and 50 characters','/Resource/image/notification_error.png','2000','sound2');
			}else if(new_password == cnf_new_pwd){
			}else{
				notify('error','New Password  and Confirm New Password should match','/Resource/image/notification_error.png','2000','sound2');
			}
		}else{
			notify('error','Confirm New Password is required','/Resource/image/notification_error.png','2000','sound2');
		}
	});

	$("#forgot_prokart_otp").on('change',function (e) {
		var otp_entered = $("#forgot_prokart_otp").val();
		if(otp_entered.length == 0 || otp_entered.length < 6 ){
			otp = false;
			notify('error','Please enter a valid OTP','/Resource/image/notification_error.png','2000','sound2');
		}else{
			if(otp_entered == 	$("#prokart_otp_response").val()){
				otp = true;
				notify('success','OTP matched successfully','/Resource/image/notification_success.png','2000','sound1');
			}else{
				otp = false;
				notify('error','Please enter a correct OTP','/Resource/image/notification_error.png','2000','sound2');
			}
		}
	});

	$("#forgot_prokart_pwd_change").click(function(e) {
		e.preventDefault();
		var pwd_match = false;
		var otp_entered = $("#forgot_prokart_otp").val();
		var new_password = $("#forgot_prokart_password").val();
		var cnf_new_pwd = $("#forgot_prokart_cnfpassword").val();
		
		if(new_password == cnf_new_pwd){
			pwd_match = true;
		}else{
			notify('error','Password and Confirm New Password should match','/Resource/image/notification_error.png','2000','sound2');
		}
		
		if(otp_entered == ''){
			otp = false;
			notify('error','Please enter your OTP first','/Resource/image/notification_error.png','2000','sound2');
		}
		
		if(email == true && otp == true && pwd_match == true){
			$("#prokart_loading_login").show();
			var formData = {
					'email' 			: $("#forgot_prokart_email").val(),
					'new_password' 		: $("#forgot_prokart_password").val(),
					'cnf_new_password' 	: $("#forgot_prokart_cnfpassword").val(),
					'entered_otp'		: $("#forgot_prokart_otp").val(),
					'sent_otp'			: $("#prokart_otp_response").val(),
					'otp_create_time'	: $("#prokart_otp_create_time").val(),					
			};

			$.ajax({
				url : 'http://192.168.0.100:7080/prokart/forgotpassword',
				contentType : "application/json",
				dataType : "json",
				type : "POST",
				data : JSON.stringify(formData),
				success : function(response,status) {
					$("#prokart_loading_login").hide();
					if(response.error_type == 'email'|| response.error_type == 'new_password' 
						|| response.error_type == 'cnf_new_password' || response.error_type == 'pwd_match_error'
							||response.error_type == 'otp_error'){
						notify('error',response.error_description,'/Resource/image/notification_error.png','2000','sound2');
					}else if(response.success_type == 'pwd__change_successfull'){
						notify('success',response.success_description,'/Resource/image/notification_success.png','2000','sound1');
					}else if(response.error_type == 'pwd__change_failed'){
						notify('error',response.error_description,'/Resource/image/notification_error.png','2000','sound2');
					}
				},
				error : function(response,status,condition) {
					$("#prokart_loading_login").hide();
					notify('error','Failed !!, Please try after some time','/Resource/image/notification_error.png','2000','sound2');
				}
			});
		}		
	});

});

function forgotPassword () {
	$("#forgot_pwd").show();
	$("#signup").hide();
	$("#login").hide();
	$("#prokart_otp").hide();
	$('#forgot_prokart_otp').val('');
}

function login(){
	$("#forgot_pwd").hide();
}

function signup(){
	$("#forgot_pwd").hide();
}

function notify(notificationType,message,imagePath,timeDelay,soundTrack){
	Lobibox.notify(
			notificationType, 				// Available types 'warning', 'info', 'success', 'error'
			{
				title: null,                // Title of notification. Do not include it for default title or set custom string. Set this false to disable title
				size: 'mini',             			// normal, mini, large
				soundPath: '/Resource/sounds/',   // The folder path where sounds are located
				soundExt: '.ogg',           // Default extension for all sounds
				showClass: 'fadeInDown',        // Show animation class.
				hideClass: 'fadeUpDown',       // Hide animation class.
				icon: true,                 // Icon of notification. Leave as is for default icon or set custom string
				msg: message,// Message of notification
				img: imagePath,                  // Image source string
				closable: true,             // Make notifications closable
				delay: timeDelay,           // Hide notification after this time (in miliseconds)
				delayIndicator: true,       // Show timer indicator
				closeOnClick: true,         // Close notifications by clicking on them
				width: 300,                 // Width of notification box
				sound: soundTrack,                // Sound of notification. Set this false to disable sound. Leave as is for default sound or set custom soud path
				position: "top right",    // Place to show notification. Available options: "top left", "top right", "bottom left", "bottom right"
				iconSource: "bootstrap",     // "bootstrap" or "fontAwesome" the library which will be used for icons
				rounded: false,             // Whether to make notification corners rounded
				messageHeight: 60 
			});
}
