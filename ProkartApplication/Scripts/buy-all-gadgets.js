﻿//======================================================================================
//************** buy-all-gadgets.js consists all main functionalities related to Buy ***
//               page.                                                               ***
//======================================================================================
//****************************** Author: Pranab Kumar De *******************************
//======================================================================================

//Declaring local and global variables
var globalPicName;
var orderAmount = 0;
var totalAmount;
var quantityArray = [];
var sortedItemArray = [];
var minValue = 0, maxValue = 20000;
var startIndex = 0, endIndex = 11;
var allAvailableItems = [];
var uniqueBrands = [];
var checkedBrands = [];
var pictureNameArray = [];
var sortedResultByPrice = [];
var sortedResultByBrandAndPrice = [];
var allAvailableBrands;
var itemIndex = [];
var result, resultByPrice, resultByBrand, finalResult, availableBrandsResult;
var numberOfrecords = 0;
var picLink = "";

//********** Document Onload function *************
//-------------------------------------------------
//-------------------------------------------------
$(document).ready(function () {
    //1.         Local variables and execute initial functions
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    var cartItems = new cookieList("item-list");
    var quantity = 1;
    for (var i = 0; i < cartItems.items().length; i++) {
        if ($.inArray(cartItems.items()[i], sortedItemArray) == -1) {
            sortedItemArray.push(cartItems.items()[i]);
        }
        else
            continue;
        for (var j = i + 1; j < cartItems.items().length; j++) {
            if (cartItems.items()[i] == cartItems.items()[j]) {
                quantity = quantity + 1;
            }
        }
        quantityArray.push(quantity);
        quantity = 1;
    }

    //Ajax Call to fill All Available Brands Array
    $.ajax({
        url: 'http://prokartwebservice.us-west-2.elasticbeanstalk.com/ProkartWebServices.svc/WebRequest/GetAllAvailableBrands',
        type: 'POST',
        async: false,
        datatype: "json",
        statusCode: {
        },
        success: function (reply) {
            availableBrandsResult = JSON.parse(reply);
            allAvailableItems = [];
            for (var b = 0; b < availableBrandsResult.length; b++) {
                allAvailableItems.push(availableBrandsResult[b].order_id);
            }
        },
        error: function (error, status, msg) {
            alert("Error while fetching mobile brands");
        }
    });
    var itemsCount = sortedItemArray.length;
    if (itemsCount > 0) {
        $("#cart").attr('class', 'cd-cart items-added');
        $("#cart span").text(itemsCount);
    }
    $.cookie.join = true;
    $.cookie.raw = true;
    itemIndex[0] = startIndex;
    itemIndex[1] = endIndex;
    ajaxCall(itemIndex);
    numberOfrecords = result.length;
    if (numberOfrecords < 12) {
        $("#show-more-div").css("display", "none");
    }
    buildHtml(result);
    buttonAnimateStyle();
    clearFilterAndFilterButtonPosition();
    //2.                    Events                         
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //*****************************************************************
    //**************** View Item event ********************************
    //-----------------------------------------------------------------
    $(".view-item").on('click', function () {
        var selected = $(this);
        var selectedId = selected.attr('id');
        var numb = selectedId.match(/\d/g);
        numb = numb.join("");
        viewItem(result, numb);        
        $("input[value='Add to Cart']").click(function () {
            var count = 0, newItem = true;
            var thisItem = $(this);            
            for (var i = 0; i < sortedItemArray.length; i++) {
                if (result[numb].order_id == sortedItemArray[i]) {
                    count = quantityArray[i];
                    newItem = false;
                }
                else
                    continue;
            }
            
            if (count < 9 || newItem == true) {
                addToCartFunctionInside(result, numb, thisItem);
            }
            if(count >= 9) {
                countMaximumModal();
            }
        });
        detailsModal();
    });

    //*****************************************************************
    //**************** Add to cart event ******************************
    //-----------------------------------------------------------------
    $(".add-to-cart").on('click', function () {
        var selected = $(this);
        var selectedId = selected.attr('id');
        var numb = selectedId.match(/\d/g);
        numb = numb.join("");
        var count = 0, newItem = true;
        for (var i = 0; i < sortedItemArray.length; i++) {
            if (result[numb].order_id == sortedItemArray[i]) {
                count = quantityArray[i];
                newItem = false;
            }
            else
                continue;
        }
        
        if (count < 9 || newItem == true) {
            addToCartFunction(result, numb);
        }
        if(count >= 9) {
            countMaximumModal();
        }
    });

    //*****************************************************************
    //******************* Cart on click event *************************
    //-----------------------------------------------------------------
    $("#cart").on('click', function () {
        document.getElementById('cart-body').innerHTML = null;
        if (quantityArray.length > 0) {
            $("#place-order-button").css("background-color", "#253fd0");
            $("#place-order-button").attr("value", "Place Order");
        }
        else {
            $("#place-order-button").css("background-color", "#9faae4");
            $("#place-order-button").attr("value", "Cart is Empty");
        }        
        totalAmount = 0;
        for (var count = 0; count < sortedItemArray.length; count++) {
            var singleItem = JSON.parse($.cookie(sortedItemArray[count]));
            pictureNameArray = [];
            if ($.inArray(singleItem.deviceId, allAvailableItems) == -1) {
                if(singleItem.picture.indexOf(',') > -1)
                    pictureNameArray = singleItem.picture.split(',')
                if (pictureNameArray.length != 0)
                    picLink = 'https://s3-us-west-2.amazonaws.com/prokart/' + pictureNameArray[0];
                else
                    picLink = 'https://s3-us-west-2.amazonaws.com/prokart/' + singleItem.picture;
                var itemHtml = '<tr>';
                itemHtml = itemHtml + '<td style="text-align:center; border-top-color: red"><a><img alt="" src="' + picLink + '" style="width:115px"></a></td>';
                itemHtml = itemHtml + '<td style="border-top-color: red">' + singleItem.name + '</br><p style="color:red">Item Sold</p></td>';
                itemHtml = itemHtml + '<td style="text-align: left; border-top-color: red"><input type="number" disabled="disabled" onkeydown="return false" maxlength="1" min="1" max="9" id="t' + singleItem.deviceId + '" value="' + quantityArray[count] + '" class="input-mini"></td>';
                itemHtml = itemHtml + '<td style="border-top-color: red">' + singleItem.unitPrice + '</td>';
                itemHtml = itemHtml + '<td style="border-top-color: red" id="single-item-total-' + singleItem.deviceId + '">' + parseInt(quantityArray[count]) * parseInt(singleItem.unitPrice) + '</td>';
                itemHtml = itemHtml + '<td style="text-align: center; border-top-color: red"><a href="#0" class="delete-cart-item" id="' + singleItem.deviceId + '"><span><img src="../Resource/image/close-button.png" style="width:6%" /></span></a></td>';
                itemHtml = itemHtml + '</tr>';
                $('#cart-body').append(itemHtml);
            }
            else {
                if (singleItem.picture.indexOf(',') > -1)
                    pictureNameArray = singleItem.picture.split(',')
                if (pictureNameArray.length != 0)
                    picLink = 'https://s3-us-west-2.amazonaws.com/prokart/' + pictureNameArray[0];
                else
                    picLink = 'https://s3-us-west-2.amazonaws.com/prokart/' + singleItem.picture;
                var itemHtml = '<tr>';
                itemHtml = itemHtml + '<td style="text-align:center"><a><img alt="" src="' + picLink + '" style="width:115px"></a></td>';
                itemHtml = itemHtml + '<td>' + singleItem.name; +'</td>';
                itemHtml = itemHtml + '<td style="text-align: left"><input type="number" onkeydown="return false" maxlength="1" min="1" max="9" id="t' + singleItem.deviceId + '" value="' + quantityArray[count] + '" class="input-mini"></td>';
                itemHtml = itemHtml + '<td>' + singleItem.unitPrice + '</td>';
                itemHtml = itemHtml + '<td id="single-item-total-' + singleItem.deviceId + '">' + parseInt(quantityArray[count]) * parseInt(singleItem.unitPrice) + '</td>';
                itemHtml = itemHtml + '<td style="text-align: center"><a href="#0" class="delete-cart-item" id="' + singleItem.deviceId + '"><span><img src="../Resource/image/close-button.png" style="width:6%" /></span></a></td>';
                itemHtml = itemHtml + '</tr>';
                $('#cart-body').append(itemHtml);
                totalAmount = totalAmount + (parseInt(quantityArray[count]) * parseInt(singleItem.unitPrice));
            }
        }
        var html = '<tr>';
        html = html + '<td>&nbsp;</td>';
        html = html + '<td>&nbsp;</td>';
        html = html + '<td>&nbsp;</td>';
        html = html + '<td>&nbsp;</td>';
        html = html + '<td><strong id="total-amount" style="font-weight: 600">' + totalAmount + '</strong></td>';
        html = html + '<td>&nbsp;</td>';
        html = html + '</tr>';
        $('#cart-body').append(html);
        $('#cart-item-modal').show();
        orderAmount = totalAmount;
        if (orderAmount > 0) {
            $("#place-order-button").css("background-color", "#253fd0");
            $("#place-order-button").attr("value", "Place Order");
        }
        else {
            $("#place-order-button").css("background-color", "#9faae4");
            $("#place-order-button").attr("value", "Cart is Empty");
        }
        var cartmodal = document.getElementById('cart-item-modal');
        var cartSpan = document.getElementsByClassName("cartclose")[0];
        var itemGallery = document.getElementById('item-gallery');
        itemGallery.style.display = "none";
        cartmodal.style.display = "block";

        cartSpan.onclick = function () {
            cartmodal.style.display = "none";
            itemGallery.style.display = "";
        }
        window.onclick = function (event) {
            if (event.target == cartmodal) {
                cartmodal.style.display = "none";
                itemGallery.style.display = "";
            }
        }
        //On deleting item from cart
        $(".delete-cart-item").on('click', function () {
            var selectedId = $(this).attr("id");
            $(this).closest('tr').remove();
            deleteCartItem(selectedId);
        });
        var previousCount = 0;
        $(".input-mini").on('input onkeyup', function () {
            var selected = $(this);
            var selectedId = String(selected.attr('id'));
            var numb = selectedId.match(/\d/g);
            numb = numb.join("");
            var thisItemOnCart = $.inArray(numb, sortedItemArray);
            previousCount = quantityArray[thisItemOnCart];
            var currentCount = document.getElementById(selectedId).value;
            if (previousCount < currentCount) {
                var addedItems = new cookieList("item-list");
                addedItems.add(numb);
            }
            if (previousCount > currentCount) {
                var addedItems = new cookieList("item-list");
                addedItems.remove(numb);
            }
            calculateNewPrice(numb, currentCount);
            
        });

    });

    //*****************************************************************
    //**************** Filter Button On click event *******************
    //-----------------------------------------------------------------
    $('#filter').on('click', function () {
        $("html, body").animate({ scrollTop: 0 }, 600);
        $('#by-price').css('opacity', '');
        $('#by-brand').css('opacity', '');
        $('#item-gallery').css('opacity', '0.6');
        $(this).css('display', 'none');
        $('#by-brand').css('display', '');
        $('#by-price').css('display', '');
        $('#filterclose').css('display', '');
        $('#filter-brands').css('display', 'none');
        $('#price-slider').css('display', 'none');
    });

    //*****************************************************************
    //**************** Close link On click event **********************
    //-----------------------------------------------------------------
    $('#filterclose').click(function () {
        $('#filter').css('display', '');
        $('#filter').removeAttr('disabled');
        $('#by-brand').css('display', 'none');
        $('#by-price').css('display', 'none');
        $(this).css('display', 'none');
        $('#filter-brands').css('display', 'none');
        $('#item-gallery').css('opacity', '');
        $('#price-slider').css('display', 'none');
    });

    //*****************************************************************
    //**************** By Brand filter On click event *****************
    //-----------------------------------------------------------------
    $('#by-brand').click(function () {
        $('#apply-brand-filter').css('display', '');
        $('#price-slider').css('display', 'none');
        $('#by-brand').css('opacity', '');
        $('#by-price').css('opacity', '0.3');
        $.ajax({
            url: 'http://prokartwebservice.us-west-2.elasticbeanstalk.com/ProkartWebServices.svc/WebRequest/GetAllAvailableBrands',
            type: 'POST',
            async: false,
            statusCode: {
            },
            success: function (reply) {
                allAvailableBrands = JSON.parse(reply);
            },
            error: function (error, status, msg) {
                alert("Error while fetching mobile brands");
            }
        });
        uniqueBrands = [];
        document.getElementById('brand-filter-table').innerHTML = null;
        for (var i = 0; i < allAvailableBrands.length; i++) {
            if ($.inArray(allAvailableBrands[i].brand.toUpperCase(), uniqueBrands) == -1) {
                uniqueBrands.push(allAvailableBrands[i].brand.toUpperCase());
                if ($.inArray(allAvailableBrands[i].order_id, sortedResultByBrandAndPrice) == -1) {
                    if (allAvailableBrands[i].price >= minValue && allAvailableBrands[i] <= maxValue) {
                        sortedResultByBrandAndPrice.push(allAvailableBrands[i].order_id);
                    }                    
                }                    
            }
            else
                continue;
        }
        var tdCount = 0;
        var html = '';
        html = html + '<tr>';
        for (var item = 0; item < uniqueBrands.length; item++) {
            if ($.inArray(uniqueBrands[item], checkedBrands) != -1)
                html = html + '<td><label class="brand-filter-label" for="brand-filter-checkbox-' + item + '" style="font-size:small"><input type="checkbox" checked="checked" id="brand-filter-checkbox-' + item + '" style="width:17px; height:17px; background:-webkit-linear-gradient(top, #fcfff4 0%, #dfe5d7 40%, #b3bead 100%); background:linear-gradient(top, #fcfff4 0%, #dfe5d7 40%, #b3bead 100%); margin:20px auto; -webkit-box-shadow:inset 0px 1px 1px white, 0px 1px 3px rgba(245, 247, 247, 0)">' + uniqueBrands[item] + '</label></td>';
            else
                html = html + '<td><label class="brand-filter-label" for="brand-filter-checkbox-' + item + '" style="font-size:small"><input type="checkbox"  id="brand-filter-checkbox-' + item + '" style="width:17px; height:17px; background:-webkit-linear-gradient(top, #fcfff4 0%, #dfe5d7 40%, #b3bead 100%); background:linear-gradient(top, #fcfff4 0%, #dfe5d7 40%, #b3bead 100%); margin:20px auto; -webkit-box-shadow:inset 0px 1px 1px white, 0px 1px 3px rgba(245, 247, 247, 0)">' + uniqueBrands[item] + '</label></td>';
            tdCount++;
            if (tdCount > 5) {
                html = html + '</tr>';
                if (item < uniqueBrands.length - 1)
                    html = html + '<tr>';
                tdCount = 0;
                continue;
            }
            if (item == uniqueBrands.length - 1) {
                html = html + '</tr>';
            }
        }
        $('#brand-filter-table').append(html);
        $('#filter-brands').css('display', '');
        //filter on checkbox click
        $("input:checkbox").change(function () {
            if ($(this).is(':checked')) {
                checkedBrands.push($("label[for='" + this.id + "']").text());
            }
            else {
                if ((index = checkedBrands.indexOf($("label[for='" + this.id + "']").text())) !== -1) {
                    checkedBrands.splice(index, 1);
                }
            }
        });
    });

    //*****************************************************************
    //**************** Brand filter Apply On click event **************
    //-----------------------------------------------------------------
    $("#apply-brand-filter").on("click", function () {
        $("#show-more-div").css("display", "none");
        if (checkedBrands.length > 0) {
            resultByBrand = null;
            var url = 'http://prokart.us-west-2.elasticbeanstalk.com/Buy/GetSortedItems';
            $.ajax({
                url: url,
                type: 'POST',
                async: false,
                data: { brands: checkedBrands },
                statusCode: {
                },
                success: function (reply) {
                    resultByBrand = JSON.parse(reply);
                    if (resultByPrice != null) {                        
                        var li;
                        document.getElementById('device-gallery').innerHTML = null;
                        for (var i = 0; i < resultByBrand.length; i++) {
                            pictureNameArray = [];
                            if ($.inArray(resultByBrand[i].order_id, sortedResultByBrandAndPrice) != -1) {
                                var picLink = "";
                                if (resultByBrand[i].picture_name == null || resultByBrand[i].picture_name == "") {
                                    picLink = 'https://s3-us-west-2.amazonaws.com/prokart/default-device-picture.jpeg';
                                }
                                else {
                                    if (resultByBrand[i].picture_name.indexOf(',') > -1)
                                        pictureNameArray = resultByBrand[i].picture_name.split(',')
                                    if (pictureNameArray.length != 0)
                                        picLink = 'https://s3-us-west-2.amazonaws.com/prokart/' + pictureNameArray[0];
                                    else
                                        picLink = 'https://s3-us-west-2.amazonaws.com/prokart/' + resultByBrand[i].picture_name;
                                }

                                li = '<li style="width:297px; height:332px; float:left">';
                                li = li + '<div class="cd-single-item">';
                                li = li + '<a>';
                                li = li + '<ul class="cd-slider-wrapper">';
                                li = li + '<li class="selected">';
                                li = li + '<img src="' + picLink + '" style="width:325px; height:300px">';
                                li = li + '</li>';
                                li = li + '</ul>';
                                li = li + '</a>';
                                li = li + '<div class="cd-customization" id="cd-customization-buttons">';
                                li = li + '<button class="view-item" id="item-' + i + '" style="padding-left:50%; background-color:#253fd0; font-size:small">';
                                li = li + '<em>View Item</em>';
                                li = li + '<svg x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32">';
                                li = li + '<path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11" />';
                                li = li + '</svg>';
                                li = li + '</button>';
                                li = li + '<button class="add-to-cart" id="cart-' + i + '" style="padding-right:50%; font-size:small">';
                                li = li + '<em>Add to Cart</em>';
                                li = li + '<svg x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32">';
                                li = li + '<path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11" />';
                                li = li + '</svg>';
                                li = li + '</button>';
                                li = li + '</div>';
                                li = li + '</div>';
                                li = li + '<div class="cd-item-info">';
                                li = li + '<b><em style="color:#262626">' + resultByBrand[i].brand + ' ' + resultByBrand[i].model + '</em></b>';
                                li = li + '<em> Rs ' + resultByBrand[i].price + '</em>';
                                li = li + '</div>';
                                li = li + '</li>';
                                $('#device-gallery').append(li);
                            }
                        }
                    }
                    else {
                        document.getElementById('device-gallery').innerHTML = null;
                        buildHtml(resultByBrand);
                    }
                },
                error: function (error, status, msg) {
                    alert("Error while fetching mobile brands");
                }
            });
        }
        else
            location.reload();
        $('#filter').css('display', '');
        $('#filter').removeAttr('disabled');
        $('#by-brand').css('display', 'none');
        $('#by-price').css('display', 'none');
        $(this).css('display', 'none');
        $('#filter-brands').css('display', 'none');
        $('#item-gallery').css('opacity', '');
        $('#price-slider').css('display', 'none');
        $('#filterclose').css('display', 'none');

        $(".view-item").on('click', function () {
            var selected = $(this);
            var selectedId = selected.attr('id');
            var numb = selectedId.match(/\d/g);
            numb = numb.join("");
            viewItem(resultByBrand, numb);
            $("input[value='Add to Cart']").click(function () {
                var count = 0, newItem = true;
                var thisItem = $(this);
                for (var i = 0; i < sortedItemArray.length; i++) {
                    if (resultByBrand[numb].order_id == sortedItemArray[i]) {
                        count = quantityArray[i];
                        newItem = false;
                    }
                    else
                        continue;
                }

                if (count < 9 || newItem == true) {
                    addToCartFunctionInside(resultByBrand, numb, thisItem);
                }
                if (count >= 9) {
                    countMaximumModal();
                }
            });
            detailsModal();
        });
        buttonAnimateStyle();
        $(".add-to-cart").on('click', function () {
            var selected = $(this);
            var selectedId = selected.attr('id');
            var numb = selectedId.match(/\d/g);
            numb = numb.join("");
            var count = 0, newItem = true;
            for (var i = 0; i < sortedItemArray.length; i++) {
                if (resultByBrand[numb].order_id == sortedItemArray[i]) {
                    count = quantityArray[i];
                    newItem = false;
                }
                else
                    continue;
            }

            if (count < 9 || newItem == true) {
                addToCartFunction(resultByBrand, numb);
            }
            if (count >= 9) {
                countMaximumModal();
            }
        });
        if (resultByBrand.length > 0) {
            $('#clear-brand-filter').css('display', '');
        }
        $('#clear-brand-filter').on('click', function () {
            location.reload();
        });
    });

    //*****************************************************************
    //**************** By Price filter On click event *****************
    //-----------------------------------------------------------------
    $('#by-price').click(function () {
        $('#price-slider').css('display', '');
        $('#by-price').css('opacity', '');
        $('#by-brand').css('opacity', '0.3');
        $('#filter-brands').css('display', 'none');
        $(function () {
            $("#slider-range").slider({
                range: true,
                min: 0,
                max: 50000,
                values: [minValue, maxValue],
                slide: function (event, ui) {
                    $("#amount").val("Rs " + ui.values[0] + "  -  Rs " + ui.values[1]);
                },
                change: function (event, ui) {
                    minValue = ui.values[0];
                    maxValue = ui.values[1];
                }
            });
            $("#amount").val("Rs " + $("#slider-range").slider("values", 0) +
              "  -  Rs " + $("#slider-range").slider("values", 1));
        });
        minValue = minValue, maxValue = maxValue;
    });

    //*****************************************************************
    //**************** Price filter Apply On click event **************
    //-----------------------------------------------------------------
    $("#apply-price-filter").click(function () {
        $("#show-more-div").css("display", "none");
        if (resultByBrand != null)
            resultByPrice = resultByBrand;
        else
            resultByPrice = result;
        sortedResultByPrice = [];
        sortedResultByBrandAndPrice = [];
        for (var item = 0; item < resultByPrice.length; item++) {
            if (resultByPrice[item].price >= minValue && resultByPrice[item].price <= maxValue) {
                sortedResultByPrice.push(resultByPrice[item].order_id);
            }
        }
        for (var branditem = 0; branditem < result.length; branditem++) {
            if (result[branditem].price >= minValue && result[branditem].price <= maxValue) {
                sortedResultByBrandAndPrice.push(result[branditem].order_id);
            }
        }
        var li;
        document.getElementById('device-gallery').innerHTML = null;
        for (var i = 0; i < resultByPrice.length; i++) {
            pictureNameArray = [];
            if ($.inArray(resultByPrice[i].order_id, sortedResultByPrice) != -1) {
                var picLink = "";
                if (resultByPrice[i].picture_name == null || resultByPrice[i].picture_name == "") {
                    picLink = 'https://s3-us-west-2.amazonaws.com/prokart/default-device-picture.jpeg';
                }
                else {
                    if (resultByPrice[i].picture_name.indexOf(',') > -1)
                        pictureNameArray = resultByPrice[i].picture_name.split(',')
                    if (pictureNameArray.length != 0)
                        picLink = 'https://s3-us-west-2.amazonaws.com/prokart/' + pictureNameArray[0];
                    else
                        picLink = 'https://s3-us-west-2.amazonaws.com/prokart/' + resultByPrice[i].picture_name;
                }

                li = '<li style="width:297px; height:332px; float:left">';
                li = li + '<div class="cd-single-item">';
                li = li + '<a>';
                li = li + '<ul class="cd-slider-wrapper">';
                li = li + '<li class="selected">';
                li = li + '<img src="' + picLink + '" style="width:325px; height:300px">';
                li = li + '</li>';
                li = li + '</ul>';
                li = li + '</a>';
                li = li + '<div class="cd-customization" id="cd-customization-buttons">';
                li = li + '<button class="view-item" id="item-' + i + '" style="padding-left:50%; background-color:#253fd0; font-size:small">';
                li = li + '<em>View Item</em>';
                li = li + '<svg x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32">';
                li = li + '<path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11" />';
                li = li + '</svg>';
                li = li + '</button>';
                li = li + '<button class="add-to-cart" id="cart-' + i + '" style="padding-right:50%; font-size:small">';
                li = li + '<em>Add to Cart</em>';
                li = li + '<svg x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32">';
                li = li + '<path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11" />';
                li = li + '</svg>';
                li = li + '</button>';
                li = li + '</div>';
                li = li + '</div>';
                li = li + '<div class="cd-item-info">';
                li = li + '<b><em style="color:#262626">' + resultByPrice[i].brand + ' ' + resultByPrice[i].model + '</em></b>';
                li = li + '<em> Rs ' + resultByPrice[i].price + '</em>';
                li = li + '</div>';
                li = li + '</li>';
                $('#device-gallery').append(li);
            }
        }
        $('#filter').css('display', '');
        $('#filter').removeAttr('disabled');
        $('#by-brand').css('display', 'none');
        $('#by-price').css('display', 'none');
        $('#filter-brands').css('display', 'none');
        $('#item-gallery').css('opacity', '');
        $('#price-slider').css('display', 'none');
        $('#filterclose').css('display', 'none');
        $(".view-item").on('click', function () {
            var selected = $(this);
            var selectedId = selected.attr('id');
            var numb = selectedId.match(/\d/g);
            numb = numb.join("");
            viewItem(resultByPrice, numb);
            $("input[value='Add to Cart']").click(function () {
                var count = 0, newItem = true;
                var thisItem = $(this);
                for (var i = 0; i < sortedItemArray.length; i++) {
                    if (resultByPrice[numb].order_id == sortedItemArray[i]) {
                        count = quantityArray[i];
                        newItem = false;
                    }
                    else
                        continue;
                }

                if (count < 9 || newItem == true) {
                    addToCartFunctionInside(resultByPrice, numb, thisItem);
                }
                if (count >= 9) {
                    countMaximumModal();
                }
            });
            detailsModal();
        });
        buttonAnimateStyle();
        $(".add-to-cart").on('click', function () {
            var selected = $(this);
            var selectedId = selected.attr('id');
            var numb = selectedId.match(/\d/g);
            numb = numb.join("");
            var count = 0, newItem = true;
            for (var i = 0; i < sortedItemArray.length; i++) {
                if (resultByPrice[numb].order_id == sortedItemArray[i]) {
                    count = quantityArray[i];
                    newItem = false;
                }
                else
                    continue;
            }

            if (count < 9 || newItem == true) {
                addToCartFunction(resultByPrice, numb);
            }
            if (count >= 9) {
                countMaximumModal();
            }
        });
        if (resultByPrice.length > 0) {
            $('#clear-brand-filter').css('display', '');
        }
        $('#clear-brand-filter').on('click', function () {
            location.reload();
        });
    });

    //*****************************************************************
    //**************** Clicking on Show More Items event **************
    //-----------------------------------------------------------------
    $("#show-more").on("click", function () {
        finalResult = result;
        itemIndex[0] = startIndex;
        itemIndex[1] = endIndex;
        ajaxCall(itemIndex);
        var numberOfrecords = result.length;
        for (var i = 0; i < result.length; i++) {
            finalResult.push(result[i]);
        }
        result = finalResult;
        buildHtml(result);
        buttonAnimateStyle();
        $(".view-item").on('click', function () {
            var selected = $(this);
            var selectedId = selected.attr('id');
            var numb = selectedId.match(/\d/g);
            numb = numb.join("");
            viewItem(result, numb);
            $("input[value='Add to Cart']").click(function () {
                var count = 0, newItem = true;
                var thisItem = $(this);
                for (var i = 0; i < sortedItemArray.length; i++) {
                    if (result[numb].order_id == sortedItemArray[i]) {
                        count = quantityArray[i];
                        newItem = false;
                    }
                    else
                        continue;
                }

                if (count < 9 || newItem == true) {
                    addToCartFunctionInside(result, numb, thisItem);
                }
                if (count >= 9) {
                    countMaximumModal();
                }
            });
            detailsModal();
        });
        $(".add-to-cart").on('click', function () {
            var selected = $(this);
            var selectedId = selected.attr('id');
            var numb = selectedId.match(/\d/g);
            numb = numb.join("");
            var count = 0, newItem = true;
            for (var i = 0; i < sortedItemArray.length; i++) {
                if (result[numb].order_id == sortedItemArray[i]) {
                    count = quantityArray[i];
                    newItem = false;
                }
                else
                    continue;
            }

            if (count < 9 || newItem == true) {
                addToCartFunction(result, numb);
            }
            if (count >= 9) {
                countMaximumModal();
            }
        });
        if (numberOfrecords < 12) {
            $("#show-more-div").css("display", "none");
        }
        $("html, body").animate({ scrollTop: $(document).height() }, "slow");
    });




    //3.                    Functions
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //**************************************************************
    //********** Function for Button Animation Style ***************
    //--------------------------------------------------------------
    function buttonAnimateStyle() {
        var productCustomization = $('.cd-customization'),
		cart = $('.cd-cart'),
		animating = false;

        initCustomization(productCustomization);

        $('body').on('click', function (event) {
            //if user clicks outside the .cd-gallery list items - remove the .hover class 
            //and close the open ul.size/ul.color list elements
            if ($(event.target).is('body') || $(event.target).is('.cd-gallery')) {
                deactivateCustomization();
            }
        });

        function initCustomization(items) {
            items.each(function () {
                var actual = $(this),
                    selectOptions = actual.find('[data-type="select"]'),
                    addToCartBtn = actual.find('.add-to-cart'),
                    touchSettings = actual.next('.cd-customization-trigger');

                //detect click on ul.size/ul.color list elements 
                selectOptions.on('click', function (event) {
                    var selected = $(this);
                    //open/close options list
                    selected.toggleClass('is-open');
                    resetCustomization(selected);

                    if ($(event.target).is('li')) {
                        // update selected option
                        var activeItem = $(event.target),
                            index = activeItem.index() + 1;

                        activeItem.addClass('active').siblings().removeClass('active');
                        selected.removeClass('selected-1 selected-2 selected-3').addClass('selected-' + index);
                        // if color has been changed, update the visible product image 
                        selected.hasClass('color') && updateSlider(selected, index - 1);
                    }
                });

                //detect click on the add-to-cart button
                addToCartBtn.on('click', function () {
                    if (!animating) {
                        //animate if not already animating
                        animating = true;
                        resetCustomization(addToCartBtn);

                        addToCartBtn.addClass('is-added').find('path').eq(0).animate({
                            //draw the check icon
                            'stroke-dashoffset': 0
                        }, 300, function () {
                            setTimeout(function () {
                                updateCart();
                                addToCartBtn.removeClass('is-added').find('em').on('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function () {
                                    //wait for the end of the transition to reset the check icon
                                    addToCartBtn.find('path').eq(0).css('stroke-dashoffset', '19.79');
                                    animating = false;
                                });

                                if ($('.no-csstransitions').length > 0) {
                                    // check if browser doesn't support css transitions
                                    addToCartBtn.find('path').eq(0).css('stroke-dashoffset', '19.79');
                                    animating = false;
                                }
                            }, 600);
                        });
                    }
                });

                //detect click on the settings icon - touch devices only
                touchSettings.on('click', function (event) {
                    event.preventDefault();
                    resetCustomization(addToCartBtn);
                });
            });
        }

        function updateSlider(actual, index) {
            var slider = actual.parent('.cd-customization').prev('a').children('.cd-slider-wrapper'),
                slides = slider.children('li');

            slides.eq(index).removeClass('move-left').addClass('selected').prevAll().removeClass('selected').addClass('move-left').end().nextAll().removeClass('selected move-left');
        }

        function resetCustomization(selectOptions) {
            //close ul.clor/ul.size if they were left open and user is not interacting with them anymore
            //remove the .hover class from items if user is interacting with a different one
            selectOptions.siblings('[data-type="select"]').removeClass('is-open').end().parents('.cd-single-item').addClass('hover').parent('li').siblings('li').find('.cd-single-item').removeClass('hover').end().find('[data-type="select"]').removeClass('is-open');
        }

        function deactivateCustomization() {
            productCustomization.parent('.cd-single-item').removeClass('hover').end().find('[data-type="select"]').removeClass('is-open');
        }
    }
    //***************************************************************
    //*******show counter if this is the first item added to the cart
    //---------------------------------------------------------------
    function updateCart() {

        var cart = $('.cd-cart');
        (!cart.hasClass('items-added')) && cart.addClass('items-added');

        var cartItems = cart.find('span'),
        text = parseInt(sortedItemArray.length);
        cartItems.text(text);
    }

    //**************************************************************
    //********** Add to cart function ******************************
    //--------------------------------------------------------------
    function addToCartFunction(result, numb) {
        var device = {
            deviceId: result[numb].order_id,
            name: result[numb].brand + " " + result[numb].model,
            picture: result[numb].picture_name,
            unitPrice: result[numb].price
        };
        $.cookie(String(result[numb].order_id), JSON.stringify(device));
        var addedItems = new cookieList("item-list");
        addedItems.add(result[numb].order_id);
        //update items Array
        createCookieandUpdateCart();
    }

    //**************************************************************
    //******* Ajax call to GetAllAvailableItems Service ************
    //--------------------------------------------------------------
    function ajaxCall(index) {
        $.ajax({
            url: 'http://prokart.us-west-2.elasticbeanstalk.com/Buy/GetAllAvailableItems',
            type: 'POST',
            async: false,
            data: { index: index },
            datatype: "application/json",
            statusCode: {
            },
            success: function (reply) {
                result = null;
                result = JSON.parse(reply);
                startIndex = endIndex + 1;
                endIndex = endIndex + 12;
            },
            error: function (error, status, msg) {
                alert("Error while fetching mobile brands");
            }
        });
    }

    //**************************************************************
    //***************** Delete cart item function ******************
    //--------------------------------------------------------------
    function deleteCartItem(selectedId) {
        var item = $.inArray(selectedId, sortedItemArray);
        var count = quantityArray[item];
        if ($.inArray(parseInt(selectedId), allAvailableItems) != -1) {
            totalAmount = totalAmount - (count * parseInt(JSON.parse($.cookie(selectedId)).unitPrice));
        }
        for (var i = 0; i < count; i++) {
            cookieList("item-list").remove(selectedId);
        }
        if (item >= 0) {
            sortedItemArray.splice(item, 1);
            quantityArray.splice(item, 1);
        }
        $.removeCookie(String(selectedId));
        if (sortedItemArray.length == 0)
            $("#cart").attr('class', 'cd-cart');
        else
            updateCart();
        if (sortedItemArray.length == 0) {
            $.removeCookie("item-list");
            sortedItemArray = [];
            quantityArray = [];
        }
        $("#total-amount").html(totalAmount);
        orderAmount = totalAmount;
        if (quantityArray.length == 0) {
            $("#place-order-button").css("background-color", "#9faae4");
            $("#place-order-button").attr("value", "Cart is Empty");
        }
        if (orderAmount == 0) {
            $("#place-order-button").css("background-color", "#9faae4");
            $("#place-order-button").attr("value", "Cart is Empty");
        }
    }

    //**************************************************************
    //***************** View Item function *************************
    //--------------------------------------------------------------
    function viewItem(result, numb) {
        pictureNameArray = [];
        globalPicName = result[numb].picture_name;
        //Load View Item Modal
        $("#item-details-modal").show();
        $("#name").text(result[numb].brand + " " + result[numb].model);
        $("#ram").text(result[numb].ram);
        $("#memory").text(result[numb].memory);
        $("#camera").text(result[numb].camera);
        $("#release-date").text(result[numb].release_date);
        $("#price").text("Rs " + result[numb].price);
        $("#condition").text((result[numb].battery_condition + result[numb].screen_quality + result[numb].charging_port_condition + result[numb].camera_condition + result[numb].heating_problem + result[numb].software_problem) / 10 + " / 10"); 

        if (result[numb].picture_name.indexOf(',') > -1)
            pictureNameArray = result[numb].picture_name.split(',')
        if (pictureNameArray.length != 0) {
            $("#main-pic").attr('src', 'https://s3-us-west-2.amazonaws.com/prokart/' + pictureNameArray[0]);
            $("#thumb1").attr('src', 'https://s3-us-west-2.amazonaws.com/prokart/' + pictureNameArray[0]);
            $("#thumb2").attr('src', 'https://s3-us-west-2.amazonaws.com/prokart/' + pictureNameArray[1]);
        }            
        else {
            if (result[numb].picture_name != ""){
                $("#main-pic").attr('src', 'https://s3-us-west-2.amazonaws.com/prokart/' + result[numb].picture_name);
                $("#thumb1").attr('src', 'https://s3-us-west-2.amazonaws.com/prokart/' + result[numb].picture_name);
                $("#thumb2").attr('src', "../Resource/image/device-picture.jpeg");
            }               
            else {
                $("#main-pic").attr('src', 'https://s3-us-west-2.amazonaws.com/prokart/default-device-picture.jpeg');
                $("#thumb1").attr('src', "../Resource/image/device-picture.jpeg");
                $("#thumb2").attr('src', "../Resource/image/device-picture.jpeg");
            }           
        }
            

        document.getElementById('details-modal-buy-section').innerHTML = null;
        html = '<input type="button" value="Add to Cart" id="details-add-' + result[numb].order_id + '" class="prokart_button" style="width:250px; height:8%; background-color:#46b29d; border-color:#8e4009" />';
        html = html + '<input type="button" value="Buy Now" class="prokart_button" style="width:250px; height:8%; background-color:#253fd0; border-color:#8e4009" />';
        $("#details-modal-buy-section").append(html);
    }

    //**************************************************************
    //**************** Calculate revised price after adding/removing
    //**************** cart items **********************************
    function calculateNewPrice(number, count) {
        var tdId = "single-item-total-" + String(number);
        var thisItem = JSON.parse($.cookie(number));
        document.getElementById(tdId).innerHTML = count * parseInt(thisItem.unitPrice);
        var item = $.inArray(number, sortedItemArray);
        var initialCount = quantityArray[item];
        var itemDiff = initialCount - count;
        quantityArray[item] = count;
        totalAmount = orderAmount;
        totalAmount = totalAmount - (itemDiff * parseInt(JSON.parse($.cookie(number)).unitPrice));
        document.getElementById("total-amount").innerHTML = totalAmount;
        orderAmount = totalAmount;
        if (orderAmount == 0) {
            $("#place-order-button").css("background-color", "#9faae4");
            $("#place-order-button").attr("value", "Cart is Empty");
        }
    }

    //**************************************************************
    //**********Filter and clear filter button positioning *********
    //--------------------------------------------------------------
    function clearFilterAndFilterButtonPosition() {
        var elementPositionFilter = $('#filter').offset();
        $(window).scroll(function () {
            if ($(window).scrollTop() > elementPositionFilter.top) {
                $('#filter').css('position', 'fixed').css('top', '30px');
            } else {
                $('#filter').css('position', 'static');
            }
        });
        var elementPositionAfter = $('#filter-division-after').offset();
        $(window).scroll(function () {
            $('#filter-division-after').css('z-index', '1000');
            if ($(window).scrollTop() > elementPositionAfter.top) {
                $('#filter-division-after').css('position', 'fixed').css('top', '30px');
            } else {
                $('#filter-division-after').css('position', 'static');
            }
        });
    }

    //**************************************************************
    //********** Add to cart function (Inside details modal) *******
    //--------------------------------------------------------------
    function addToCartFunctionInside(result, numb, thisItem) {
        var device = {
            deviceId: result[numb].order_id,
            name: result[numb].brand + " " + result[numb].model,
            picture: result[numb].picture_name,
            unitPrice: result[numb].price
        };
        $.cookie(String(result[numb].order_id), JSON.stringify(device));
        var addedItems = new cookieList("item-list");
        addedItems.add(result[numb].order_id);
        //update items Array
        createCookieandUpdateCart();
        thisItem.attr('value', 'Item Added');
        thisItem.attr('disabled', 'disabled');
        thisItem.css('box-shadow', '2px 2px 7px 2px #04443e');
        setTimeout(function () {
            $("input[value='Item Added']").attr('value', 'Add to Cart');
            $("input[value='Add to Cart']").removeAttr('disabled');
            $("input[value='Add to Cart']").css('box-shadow', 'none');
        }, 700);
    }

    //**************************************************************
    //********** Details modal function ****************************
    //--------------------------------------------------------------
    function detailsModal() {
        var detailsmodal = document.getElementById('item-details-modal');
        var span = document.getElementsByClassName("itemDetailsclose")[0];
        var itemGallery = document.getElementById('item-gallery');
        itemGallery.style.display = "none";
        detailsmodal.style.display = "block";
        span.onclick = function () {
            detailsmodal.style.display = "none";
            itemGallery.style.display = "";
        }
        window.onclick = function (event) {
            if (event.target == detailsmodal) {
                detailsmodal.style.display = "none";
                itemGallery.style.display = "";
            }
        }
    }

    //**************************************************************
    //********** Build device Gallery HTML *************************
    //--------------------------------------------------------------
    function buildHtml(result) {
        var li;
        document.getElementById('device-gallery').innerHTML = null;
        for (var i = 0; i < result.length; i++) {
            pictureNameArray = [];
            var picLink = "";
            if (result[i].picture_name == null || result[i].picture_name == "") {
                picLink = 'https://s3-us-west-2.amazonaws.com/prokart/default-device-picture.jpeg';
            }
            else {
                if (result[i].picture_name.indexOf(',') > -1)
                    pictureNameArray = result[i].picture_name.split(',')
                if (pictureNameArray.length != 0)
                    picLink = 'https://s3-us-west-2.amazonaws.com/prokart/' + pictureNameArray[0];
                else
                    picLink = 'https://s3-us-west-2.amazonaws.com/prokart/' + result[i].picture_name;
            }

            li = '<li style="width:297px; height:332px; float:left">';
            li = li + '<div class="cd-single-item">';
            li = li + '<a>';
            li = li + '<ul class="cd-slider-wrapper">';
            li = li + '<li class="selected">';
            li = li + '<img src="' + picLink + '" style="width:325px; height:300px">';
            li = li + '</li>';
            li = li + '</ul>';
            li = li + '</a>';
            li = li + '<div class="cd-customization" id="cd-customization-buttons">';
            li = li + '<button class="view-item" id="item-' + i + '" style="padding-left:50%; background-color:#253fd0; font-size:small">';
            li = li + '<em>View Item</em>';
            li = li + '<svg x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32">';
            li = li + '<path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11" />';
            li = li + '</svg>';
            li = li + '</button>';
            li = li + '<button class="add-to-cart" id="cart-' + i + '" style="padding-right:50%; font-size:small">';
            li = li + '<em>Add to Cart</em>';
            li = li + '<svg x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32">';
            li = li + '<path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11" />';
            li = li + '</svg>';
            li = li + '</button>';
            li = li + '</div>';
            li = li + '</div>';
            li = li + '<div class="cd-item-info">';
            li = li + '<b><em style="color:#262626">' + result[i].brand + ' ' + result[i].model + '</em></b>';
            li = li + '<em> Rs ' + result[i].price + '</em>';
            li = li + '</div>';
            li = li + '</li>';
            $("#device-gallery").append(li);
        }
    }


    //**************************************************************
    //************* Modal for maximum Item *************************
    //--------------------------------------------------------------
    function countMaximumModal(){
        var limitmodal = document.getElementById("no-more-items-modal");
        var limitSpan = document.getElementById("no-more-items-close");
        limitmodal.style.display = "block";
        setTimeout(function () { limitmodal.style.display = "none"; }, 1500);
        limitSpan.onclick = function () {
            limitmodal.style.display = "none";
        }
        window.onclick = function (event) {
            if (event.target == limitmodal) {
                limitmodal.style.display = "none";
            }
        }       
    }
});

//********** Functions outside document on load function ************
//-------------------------------------------------------------------

//1. createCookieandUpdateCart() will create new cookie on items addition and update the number 
//on top of cart icon
function createCookieandUpdateCart() {
    var cartItems = new cookieList("item-list");
    quantityArray = [];
    sortedItemArray = [];
    var quantity = 1;
    for (var i = 0; i < cartItems.items().length; i++) {
        if ($.inArray(cartItems.items()[i], sortedItemArray) == -1) {
            sortedItemArray.push(cartItems.items()[i]);
        }
        else
            continue;
        for (var j = i + 1; j < cartItems.items().length; j++) {
            if (cartItems.items()[i] == cartItems.items()[j]) {
                quantity = quantity + 1;
            }
        }
        quantityArray.push(quantity);
        quantity = 1;
    }
    var itemsCount = sortedItemArray.length;
    if (itemsCount > 0) {
        $("#cart").attr('class', 'cd-cart items-added');
        $("#cart span").text(itemsCount);
    }
}

//2. cookieList() will add/remove a new item to.from cookie list/array
//and check existing cookie
function cookieList(cookieName) {
    var cookie = $.cookie(cookieName);
    var items = cookie ? cookie.split(/,/) : new Array();
    return {
        "add": function (val) {
            items.push(val);
            $.cookie(cookieName, items.join(','));
        },
        "remove": function (val) {
            indx = items.indexOf(val);
            if (indx != -1) items.splice(indx, 1);
            $.cookie(cookieName, items.join(','));
        },
        "clear": function () {
            items = null;
            $.cookie(cookieName, null);
        },
        "items": function () {
            return items;
        }
    }
}

//3. Below functions will change thumbnail picture of device while on details modal page.
function changeThumbOne() {
    if (pictureNameArray.length != 0)
        $("#main-pic").attr('src', 'https://s3-us-west-2.amazonaws.com/prokart/' + pictureNameArray[0]);
    else {
        if (globalPicName != "") {
            $("#main-pic").attr('src', 'https://s3-us-west-2.amazonaws.com/prokart/' + globalPicName);
        }
        else
            $("#main-pic").attr('src', "../Resource/image/device-picture.jpeg");
    }
        
    
}
function changeThumbTwo() {
    if (pictureNameArray.length != 0)
        $("#main-pic").attr('src', 'https://s3-us-west-2.amazonaws.com/prokart/' + pictureNameArray[1]);
    else
        $("#main-pic").attr('src', "../Resource/image/device-picture.jpeg");
}
//-------------------------------------------------------------------
//-------------------- buy-all-gadgets.js ends here -----------------