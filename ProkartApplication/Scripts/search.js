//======================================================================================
//************** search.js helps to search mobile device from Internet based on ********
//************** user input.                                                    ********
//                                                                              ********
//======================================================================================
//****************************** Author: Pranab Kumar De *******************************
//======================================================================================

$(document).ready(function () {
    $("#prokart-loading-search").hide();
    $("#brand").val("");
    $("#model").val("");
    $(".brand").focus(function () {
        $(".brand-help").slideDown(500);
    }).blur(function () {
        $(".brand-help").slideUp(500);
    });


    $(".model").focus(function () {
        $(".model-help").slideDown(500);
    }).blur(function () {
        $(".model-help").slideUp(500);
    });


    var d = new Date();
    var cacheStr = "";
    cacheStr += "" + d.getMonth() + "" + Math.round((d.getDate() * 24 + d.getHours()) / 6);

    //*** declaring the local variables
    var brands = [];
    var models = [];
    var brandIds = [];
    var modelIds = [];
    var pictureLinks = [];

    //** Ajax call to gsmarena.com to get a list of all mobile devices
    //** in their database *******************************************
    //****************************************************************
    $.ajax({
        url: 'http://www.gsmarena.com/quicksearch-' + cacheStr + '.jpg',
        type: 'GET',
        async: false,
        datatype: "json",
        statusCode: {
        },
        success: function (result) {

            //Filling the arrays
            brands[0] = "";
            var i = 1;
            while (result[0][i]) {
                brands.push(result[0][i].toUpperCase());
                i++;
            }
            for (var j = 0; j < result[1].length; j++) {
                models.push(result[1][j][2].toUpperCase());
            }
            for (var k = 0; k < result[1].length; k++) {
                brandIds.push(result[1][k][0]);
            }
            for (var l = 0; l < result[1].length; l++) {
                modelIds.push(result[1][l][1]);
            }
            for (var m = 0; m < result[1].length; m++) {
                pictureLinks.push(result[1][m][4]);
            }
        },
        error: function (error, status, msg) {
            alert("Error while fetching mobile brands");
        }
    });


    $("#model").prop("disabled", true);//Disabling the model textbox at page load
    var selectedBrandID; //Variable to store brand id of input brand

    //Function for brand input
    //************************
    $("#brand").on('input select', function () {
        $("#model").val('');//Clearing model text at brand text change
        var brandTextInput = $("#brand").val().toUpperCase();
        var sortedBrandsAfterTextChange = [];
        var j = brandTextInput.length;
        selectedBrandID = 0; //initializing Selected brand ID as '0'
        //Sorting the specific brands from input
        //**************************************
        for (var i = 1; i < brands.length; i++) {
            if (j != 0) {
                if (brandTextInput === brands[i].substring(0, j)) {
                    sortedBrandsAfterTextChange.push(brands[i]);
                    selectedBrandID = i;
                }
            }
        }

        //Autocomplete Brand item
        //***********************
        $("#brand").autocomplete({
            source: function (request, response) {
                var results = $.ui.autocomplete.filter(sortedBrandsAfterTextChange, request.term);

                response(results.slice(0, 6));
            },
            minLength: 0,
            select: function (event, ui) {
                event.preventDefault();
                $("#brand").val(ui.item.label);
                brandTextInput = $("#brand").val();
                $("#brand").select();
            }
        }).focus(function () {
            $(this).autocomplete("search", "");
        });
        $("#brand").autocomplete("search", "");

        //Brand input validation on focus change
        //**************************************
        var match = $.inArray(brandTextInput, brands);
        if (match == -1 || match == 0) {
            $("#brand-error").html("<span class='brand-error'>Please enter your device brand properly</span>");
        }
        else {
            $("#brand-error").html("");
        }

        //Disabling the model textbox if brand text is empty or wrong
        if ($('#brand-error').children().is("span") || $("#brand").val().length == 0) {
            $("#model").prop("disabled", true);
        }
        else {
            $("#model").prop("disabled", false);
        }
    });

    //Function for model input
    //************************
    $("#model").on('input select', function () {
        var modelTextInput = $("#model").val().toUpperCase();
        var sortedModelsAfterTextChange = [];
        var j = modelTextInput.length;
        var modelsForSelectedBrand = [];

        //Updating model array for selected brand
        //***************************************
        for (var index = 0 ; index < brandIds.length ; index++) {
            if (brandIds[index] == selectedBrandID) {
                modelsForSelectedBrand.push(models[index]);
            }
        }

        //Sorting the specific brands from input
        //**************************************
        for (var i = 0; i < modelsForSelectedBrand.length; i++) {
            if (j != 0) {
                if (modelTextInput == modelsForSelectedBrand[i].substring(0, j)) {
                    sortedModelsAfterTextChange.push(modelsForSelectedBrand[i]);
                }
            }
        }

        //Autocomplete model item
        //***********************
        $("#model").autocomplete({
            source: function (request, response) {
                var results = $.ui.autocomplete.filter(sortedModelsAfterTextChange, request.term);

                response(results.slice(0, 6));
            },
            minLength: 0,
            select: function (event, ui) {
                event.preventDefault();
                $("#model").val(ui.item.label);
                modelTextInput = $("#model").val();
                $("#model").select();
            }
        }).focus(function () {
            $(this).autocomplete("search", "");
        });
        $("#model").autocomplete("search", "");
        //Model input validation on text change
        //*************************************
        var matchByModel = $.inArray(modelTextInput, models);
        var matchBySpecificBrand = $.inArray(modelTextInput, modelsForSelectedBrand);
        if (modelTextInput.length == 0) {
            $("#model-error").html("<span class='model-error'>Please enter your device model</span>");
        }
        else if (matchByModel == -1) {
            $("#model-error").html("<span class='model-error'>Please enter your device model properly</span>");
        }
        else if (matchBySpecificBrand == -1) {
            $("#model-error").html("<span class='model-error'>Entered device model does not belong to brand specified</span>");
        }
        else {
            $("#model-error").html("");
        }
    });

    //******* Button on-submit function *****
    //******* Validation for proper input ***
    //***************************************
    $("#check_search").on('click', function () {
        if ($("#brand").val().length == 0 && $("#model").val().length == 0) {
            $('#brand').css('border-color', 'red');
            $('#model').css('border-color', 'red');
            setTimeout(function () { $("#brand").css('border-color', ''); }, 3000);
            setTimeout(function () { $("#model").css('border-color', ''); }, 3000);
            return false;
        }
        else if ($('#brand-error').children().is("span")) {
            $('#brand').css('border-color', 'red');
            setTimeout(function () { $("#brand").css('border-color', ''); }, 3000);
            return false;
        }
        else if ($('#model-error').children().is("span")) {            
            $('#model').css('border-color', 'red');
            setTimeout(function () { $("#model").css('border-color', ''); }, 3000);
            return false;
        }
        else if ($("#brand").val().length == 0) {
            $('#brand').css('border-color', 'red');
            setTimeout(function () { $("#brand").css('border-color', ''); }, 3000);
            return false;
        }
        else if ($("#model").val().length == 0) {
            $('#model').css('border-color', 'red');
            setTimeout(function () { $("#model").css('border-color', ''); }, 3000);
            return false;
        }

        else {
            $("#prokart-loading-search").show();
            for (var index = 0 ; index < models.length ; index++) {
                if (models[index] == $("#model").val().toUpperCase() && brandIds[index] == selectedBrandID) {
                    document.getElementById('modelid').value = modelIds[index];
                    document.getElementById('picturelink').value = pictureLinks[index];
                }
            }           
            setTimeout(ajaxCall, 2500); //***ajax call after 2.5 sec delay***
        }
    });

    //***** Ajax call function to controller method to pass on the information
    //************************************************************************
    function ajaxCall(){
        var data = {
            brand: $("#brand").val(),
            model: $("#model").val(),
            modelId: $("#modelid").val(),
            pictureLink: $("#picturelink").val(),
            ram: "",
            memory: "",
            camera: "",
            age: ""
        };
        $.ajax({
            url: 'http://prokart.us-west-2.elasticbeanstalk.com/Sell/Search',
            data: data,
            type: 'POST',
            async: false,
            contenttype: "application/json",
            datatype: "json",
            success: function (result) {
                $("#specification-popup-body").load("http://prokart.us-west-2.elasticbeanstalk.com/Sell/Specification");
                $("#specification-modal").show();
                var specmodal = document.getElementById('specification-modal');
                var span = document.getElementsByClassName("specclose")[0];
                specmodal.style.display = "block";
                $("#prokart-loading-search").hide();
                span.onclick = function () {
                    specmodal.style.display = "none";
                }
                window.onclick = function (event) {
                    if (event.target == specmodal) {
                        specmodal.style.display = "none";
                    }
                }
            },
            error: function (error, status, msg) {
                alert("Error");
            }
        });

    }
});