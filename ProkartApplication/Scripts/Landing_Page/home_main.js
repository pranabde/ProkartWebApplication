﻿$(document).ready(function () {
    $("#login_signup").on('click', function () {
        $("#login-popup-body").load("http://prokart.us-west-2.elasticbeanstalk.com/Login/Login");
        $("#login-modal").show();
        var loginmodal = document.getElementById('login-modal');
        var span = document.getElementsByClassName("loginclose")[0];
        loginmodal.style.display = "none";
        setTimeout(LoadLoginModal, 50);
        span.onclick = function () {
            loginmodal.style.display = "none";
        }
        window.onclick = function (event) {
            if (event.target == loginmodal) {
                loginmodal.style.display = "none";
            }
        }
        function LoadLoginModal() {
            loginmodal.style.display = "block";
        }
    });

    $("#sell_item").on('click', function () {
        var url = 'http://prokart.us-west-2.elasticbeanstalk.com/Sell/Search';
        window.open(url);
    });
    $("#device_gallery_one").on('click', function () {
        var url = 'http://prokart.us-west-2.elasticbeanstalk.com/Buy/AllGadgets';
        window.open(url);
    });
    $("#device_gallery_two").on('click', function () {
        var url = 'http://prokart.us-west-2.elasticbeanstalk.com/Buy/AllGadgets';
        window.open(url);
    });
    $("#login_signup").hover(function () {
        $(this).css("color", "#1ab188");
    }, function () {
        $(this).css("color", "");
    });
    $("#sell_item").hover(function () {
        $(this).css("color", "#1ab188");
    }, function () {
        $(this).css("color", "");
    });
    $("#device_gallery_one").hover( function () {
        $(this).css("color", "#1ab188");
    }, function () {
        $(this).css("color", "");
    });

});