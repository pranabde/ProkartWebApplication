//======================================================================================
//************** greetings.js to greet user after successful order placement. It *******
//************** shows order id. A mail will be triggered simultaniously.        *******
//                                                                               *******
//======================================================================================
//****************************** Author: Pranab Kumar De *******************************
//======================================================================================

$(document).ready(function () {
    //Getting values from hidden field, which is controllr session variable
    var fromController = null;
    var fromControllerJson = $("#from-controller-checkout").val();
    fromController = JSON.parse(fromControllerJson);

    var message;
    var userName;

    message = fromController.Message;
    userName = fromController.FirstName;

    //******* Assigning values of variables to HTML element 
    //******* to construct the greetings message. *********
    document.getElementById("name").innerHTML = 'Hi ' + userName + ' ,';
    document.getElementById("message").innerHTML = message;    
})