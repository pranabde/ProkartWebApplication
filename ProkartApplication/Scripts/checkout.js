//======================================================================================
//************** checkout.js takes user information such as, first name          *******
//************** last name, mobile number, email address, postal address, postal *******
//************** code for further communication.                                 *******
//************** Future extension: Check if user is already logged in. If yes    *******
//************** then already known fields will be auto filled.                  *******
//                                                                               *******
//======================================================================================
//****************************** Author: Pranab Kumar De *******************************
//======================================================================================

$(document).ready(function () {

    //Getting values from hidden field, which is controllr session variable
    var fromController = null;
    var fromControllerJson = $("#from-controller-price_estimate").val();
    fromController = JSON.parse(fromControllerJson);

    $('#prokart-loading-checkout').hide();
    //Declaring local variables
    var brand, model, ram, memory, camera, releaseDate, price;
    var batteryCondition, screenQuality, chargingPortCondition, cameraCondition, heatingProblem, softwareProblem;
    var pictureName = null;

    brand = fromController.Brand;
    model = fromController.Model;
    ram = fromController.Ram;
    memory = fromController.Memory;
    camera = fromController.Camera;
    releaseDate = fromController.ReleaseDate;
    price = fromController.Price;
    batteryCondition = fromController.BatteryCondition;
    screenQuality = fromController.ScreenQuality;
    chargingPortCondition = fromController.ChargingPortCondition;
    cameraCondition = fromController.CameraCondition;
    heatingProblem = fromController.HeatingProblem;
    softwareProblem = fromController.SoftwareProblem;
    pictureName = fromController.PictureName;

    var name_regex = /^[a-zA-Z]+$/; //Regex expression for name validation
    var email_regex = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/; //Regex expression for email validation
    var number_regex = /^[0-9]+$/; //Regex expression for number validation
    var firstname = "";
    var lastname = "";
    var email = "";
    var mobile = 0;
    var addr1 = "";
    var addr2 = "";
    var pin = 0;

    //******* 'Proceed' button click function *****
    //******* validating proper input from user ***
    //*********************************************
    $("#proceed-greetings").on('click', function () {
        firstname = $('#f-name').val();
        lastname = $('#l-name').val();
        email = $('#email').val();
        mobile = $('#mobile').val();
        addr1 = $('#address1').val();
        addr2 = $('#address2').val();
        pin = $('#pin').val();

        if (firstname.length == 0 || !firstname.match(name_regex)) {
            $('#f-name').css('border-color', 'red');
            $('#f-name').attr("placeholder", "Type your First Name Here");
            setTimeout(function () { $("#f-name").css('border-color', ''); }, 3000);
            setTimeout(function () { $('#f-name').attr("placeholder", "First Name"); }, 3000);
        }
        else if (lastname.length == 0 || !lastname.match(name_regex)) {
            $('#l-name').css('border-color', 'red');
            $('#l-name').attr("placeholder", "Type your Last Name Here");
            setTimeout(function () { $("#l-name").css('border-color', ''); }, 3000);
            setTimeout(function () { $('#l-name').attr("placeholder", "Last Name"); }, 3000);
        }
        else if (email.length == 0 || !email.match(email_regex)) {
            $('#email').css('border-color', 'red');
            $('#email').attr("placeholder", "Type your Email Address Here");
            setTimeout(function () { $("#email").css('border-color', ''); }, 3000);
            setTimeout(function () { $('#email').attr("placeholder", "Email Address"); }, 3000);
        }
        else if (mobile.length == 0 || !mobile.match(number_regex) || mobile.length != 10) {
            $('#mobile').css('border-color', 'red');
            $('#mobile').attr("placeholder", "Type your Mobile Number Here");
            setTimeout(function () { $("#mobile").css('border-color', ''); }, 3000);
            setTimeout(function () { $('#mobile').attr("placeholder", "Mobile Number"); }, 3000);
        }
        else if (addr1.length == 0) {
            $('#address1').css('border-color', 'red');
            $('#address1').attr("placeholder", "Address is required");
            setTimeout(function () { $("#address1").css('border-color', ''); }, 3000);
            setTimeout(function () { $('#address1').attr("placeholder", "Address Line 1"); }, 3000);
        }
        else if (addr2.length == 0) {
            $('#address2').css('border-color', 'red');
            $('#address2').attr("placeholder", "Address is required");
            setTimeout(function () { $("#address2").css('border-color', ''); }, 3000);
            setTimeout(function () { $('#address2').attr("placeholder", "Address Line 2"); }, 3000);
        }
        else if (pin.length == 0 || !pin.match(number_regex) || pin.length != 6) {
            $('#pin').css('border-color', 'red');
            $('#pin').attr("placeholder", "Type your Area PIN Code Here");
            setTimeout(function () { $("#pin").css('border-color', ''); }, 3000);
            setTimeout(function () { $('#pin').attr("placeholder", "PIN Code"); }, 3000);
        }
        else if (!$('input.checkbox_check').is(':checked')) {
            alert("You need to read and accept Terms and Conditions");
        }
        else {
            $("#proceed-greetings").prop('disabled', true);
            $('#proceed-greetings').attr("value", "Please Wait");
            $("#proceed-greetings").css('background-color', '#b3b3b3');
            $('#prokart-loading-checkout').show();
            setTimeout(ajaxCall, 2500); //***ajax call after 2.5 sec delay***
        }
    });

    //***** Ajax call function to controller method to pass on the information
    //************************************************************************
    function ajaxCall() {
        var data = {
            brand: brand,
            model: model,
            ram: ram,
            memory: memory,
            camera: camera,
            releaseDate: releaseDate,
            price: price,
            firstName: firstname,
            lastName: lastname,
            email: email,
            mobile: mobile,
            addressOne: addr1,
            addressTwo: addr2,
            pin: pin,
            batteryCondition: batteryCondition,
            screenQuality: screenQuality,
            chargingPortCondition: chargingPortCondition,
            cameraCondition: cameraCondition,
            heatingProblem: heatingProblem,
            softwareProblem: softwareProblem,
            pictureName: pictureName
        };
        $.ajax({
            url: 'http://prokart.us-west-2.elasticbeanstalk.com/Sell/CheckOut',
            data: data,
            type: 'POST',
            async: false,
            contenttype: "application/json",
            datatype: "json",
            success: function (result) {
                $("#greetings-popup-body").load("http://prokart.us-west-2.elasticbeanstalk.com/Sell/Greetings");
                $("#greetings-modal").show();
                var greetingsmodal = document.getElementById('greetings-modal');
                var checkoutmodal = document.getElementById('checkout-modal');
                var pricemodal = document.getElementById('price-modal');
                var questmodal = document.getElementById('questionnaire-modal');
                var specmodal = document.getElementById('specification-modal');
                var span = document.getElementsByClassName("greetingsclose")[0];
                greetingsmodal.style.display = "block";
                $("#prokart-loading-checkout").hide();
                span.onclick = function () {
                    greetingsmodal.style.display = "none";
                    checkoutmodal.style.display = "none";
                    pricemodal.style.display = "none";
                    questmodal.style.display = "none";
                    specmodal.style.display = "none";
                }
                window.onclick = function (event) {
                    if (event.target == greetingsmodal) {
                        greetingsmodal.style.display = "none";
                        checkoutmodal.style.display = "none";
                        pricemodal.style.display = "none";
                        questmodal.style.display = "none";
                        specmodal.style.display = "none";
                    }
                }
            },
            error: function (error, status, msg) {
                $("#proceed-greetings").prop('disabled', false);
                $('#prokart-loading-checkout').hide();
                alert("Error");
            }
        });
    }
});