//======================================================================================
//************** brand-tire.js helps admin to divide all mobile device brands into  ****
//************** 3 categories(tires) based on brand value and market price.         ****
//                                                                                  ****
//======================================================================================
//****************************** Author: Pranab Kumar De *******************************
//======================================================================================

$(document).ready(function () {
    var getBrands = [];
    var brands = [];
    var brandsFromDb = [];
    var brandsToUpdate = [];
    
    //**** Ajax call to controller to retrive data
    //**** from database *************************
    //********************************************
    $.ajax({
        url: 'http://prokartwebservice.us-west-2.elasticbeanstalk.com/ProkartWebServices.svc/WebRequest/GetBrands',
        type: 'POST',
        async: false,
        datatype: "json",
        statusCode: {
        },
        success: function (result) {
            result = JSON.parse(result);
            //Filling the dropdowns
            var sel1 = document.getElementById('tier1');
            var sel2 = document.getElementById('tier2');
            var sel3 = document.getElementById('tier3');
            for (var i = 0; i < result.Tier1.length; i++) {
                var opt = document.createElement('option');
                opt.innerHTML = result.Tier1[i];
                opt.value = result.Tier1[i];
                sel1.appendChild(opt);
                brandsFromDb.push(result.Tier1[i]);
            }
            for (var i = 0; i < result.Tier2.length; i++) {
                var opt = document.createElement('option');
                opt.innerHTML = result.Tier2[i];
                opt.value = result.Tier2[i];
                sel2.appendChild(opt);
                brandsFromDb.push(result.Tier2[i]);
            }
            for (var i = 0; i < result.Tier3.length; i++) {
                var opt = document.createElement('option');
                opt.innerHTML = result.Tier3[i];
                opt.value = result.Tier3[i];
                sel3.appendChild(opt);
                brandsFromDb.push(result.Tier3[i]);                
            }            
        },
        error: function (error, status, msg) {
            alert("Error while fetching mobile brands");
        }
    });


    $("#get").on('click', function () {
        var d = new Date();
        var cacheStr = "";
        cacheStr += "" + d.getMonth() + "" + Math.round((d.getDate() * 24 + d.getHours()) / 6);

        //**** Ajax call to gsmarena.com to retrive data **********
        //**** Reson: to update the list with any new brand arrival
        //*********************************************************
        $.ajax({
            url: 'http://www.gsmarena.com/quicksearch-' + cacheStr + '.jpg',
            type: 'GET',
            async: false,
            datatype: "json",
            statusCode: {
            },
            success: function (result) {
                //Filling the arrays
                getBrands[0] = "";
                var i = 1;
                while (result[0][i]) {
                    getBrands.push(result[0][i].toUpperCase());
                    i++;
                }
                for (var i = 0; i < getBrands.length-1; i++) {
                    brands.push(getBrands[i + 1]);
                }
            },
            error: function (error, status, msg) {
                alert("Error while fetching mobile brands");
            }
        });
        jQuery.grep(brands, function (el) {
            if (jQuery.inArray(el, brandsFromDb) == -1) brandsToUpdate.push(el);
        });
        if (brandsToUpdate.length != 0) {
            var sel = document.getElementById('tier3');
            for (var i = 0; i < brandsToUpdate.length; i++) {
                var opt = document.createElement('option');
                opt.innerHTML = brandsToUpdate[i];
                opt.value = brandsToUpdate[i];
                sel.appendChild(opt);
            }
        }
             
    });
    //***** Moving tire3 brand to tire1 or tire2
    //******************************************
    $("#change3").click(function () {
        var brand = $("#tier3 option:selected").text();
        var tier = $("#tier3option option:selected").text();
        if (brand == "") {
            alert("Select a brand 1st");
        }
        else {
            if (tier == "Tier 1") {
                var sel = document.getElementById('tier1');
                var opt = document.createElement('option');
                opt.innerHTML = brand;
                opt.value = brand;
                sel.appendChild(opt);
            }
            if (tier == "Tier 2") {
                var sel = document.getElementById('tier2');
                var opt = document.createElement('option');
                opt.innerHTML = brand;
                opt.value = brand;
                sel.appendChild(opt);
            }
            $("#tier3 option:selected").remove();
        }
    });
    //***** Moving tire2 brand to tire1 or tire3
    //******************************************
    $("#change2").click(function () {
        var brand = $("#tier2 option:selected").text();
        var tier = $("#tier2option option:selected").text();
        if (brand == "") {
            alert("Select a brand 1st");
        }
        else {
            if (tier == "Tier 1") {
                var sel = document.getElementById('tier1');
                var opt = document.createElement('option');
                opt.innerHTML = brand;
                opt.value = brand;
                sel.appendChild(opt);
            }
            if (tier == "Tier 3") {
                var sel = document.getElementById('tier3');
                var opt = document.createElement('option');
                opt.innerHTML = brand;
                opt.value = brand;
                sel.appendChild(opt);
            }
            $("#tier2 option:selected").remove();
        }
    });
    //***** Moving tire1 brand to tire2 or tire3
    //******************************************
    $("#change1").click(function () {
        var brand = $("#tier1 option:selected").text();
        var tier = $("#tier1option option:selected").text();
        if (brand == "") {
            alert("Select a brand 1st");
        }
        else {
            if (tier == "Tier 2") {
                var sel = document.getElementById('tier2');
                var opt = document.createElement('option');
                opt.innerHTML = brand;
                opt.value = brand;
                sel.appendChild(opt);
            }
            if (tier == "Tier 3") {
                var sel = document.getElementById('tier3');
                var opt = document.createElement('option');
                opt.innerHTML = brand;
                opt.value = brand;
                sel.appendChild(opt);
            }
            $("#tier1 option:selected").remove();
        }
    });

    //***** Ajax call for updating brands with respective tires to database
    //*********************************************************************
    $("#update").click(function () {
        var tier1 = [];
        var tier2 = [];
        var tier3 = [];
        $("#tier1 option").each(function (i, item) {
            tier1.push(item.value);
        });
        $("#tier2 option").each(function (i, item) {
            tier2.push(item.value);
        });
        $("#tier3 option").each(function (i, item) {
            tier3.push(item.value);
        });

        var data = {
            Tier1: tier1,
            Tier2: tier2,
            Tier3: tier3
        };
        var url = 'http://prokart.us-west-2.elasticbeanstalk.com/Admin/Index';
        $.post(
            url,
            data
            );
    });
});