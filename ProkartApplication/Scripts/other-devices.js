//======================================================================================
//************** other-devices.js helps user to fill in mobile device specification ****
//************** manually, if he is unable to find his device's brand and/or model  ****
//************** at 'Search' page.                                                  ****
//                                                                                  ****
//======================================================================================
//****************************** Author: Pranab Kumar De *******************************
//======================================================================================

$(document).ready(function () {
    $('#prokart-loading-other').hide();
    var d = new Date();
    var cacheStr = "";
    cacheStr += "" + d.getMonth() + "" + Math.round((d.getDate() * 24 + d.getHours()) / 6);
    var brands = [];

    //** Ajax call to gsmarena.com to get a list of all mobile devices
    //** in their database *******************************************
    //****************************************************************
    $.ajax({
        url: 'http://www.gsmarena.com/quicksearch-' + cacheStr + '.jpg',
        type: 'GET',
        async: false,
        datatype: "json",
        statusCode: {
        },
        success: function (result) {

            //Filling the arrays
            brands[0] = "";
            var i = 1;
            while (result[0][i]) {
                brands.push(result[0][i].toUpperCase());
                i++;
            }
        },
        error: function (error, status, msg) {
            alert("Error while fetching mobile brands");
        }
    });

    $("#other-model").prop("disabled", true);//Disabling the model textbox at page load
    var selectedBrandID; //Variable to store brand id of input brand
    //Function for brand input
    //************************
    $("#other-brand").on('input select', function () {
        $("#other-model").val('');//Clearing model text at brand text change
        var brandTextInput = $("#other-brand").val().toUpperCase();
        var sortedBrandsAfterTextChange = [];
        var j = brandTextInput.length;
        selectedBrandID = 0; //initializing Selected brand ID as '0'

        //Sorting the specific brands from input
        //**************************************
        for (var i = 1; i < brands.length; i++) {
            if (j != 0) {
                if (brandTextInput === brands[i].substring(0, j)) {
                    sortedBrandsAfterTextChange.push(brands[i]);
                    selectedBrandID = i;
                }
            }
        }

    //Autocomplete Brand item
    //***********************
    $("#other-brand").autocomplete({
        source: function (request, response) {
            var results = $.ui.autocomplete.filter(sortedBrandsAfterTextChange, request.term);

            response(results.slice(0, 6));
        },
        minLength: 0,
        select: function (event, ui) {
            event.preventDefault();
            $("#other-brand").val(ui.item.label);
            brandTextInput = $("#other-brand").val();
            $("#other-brand").select();
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
    $("#other-brand").autocomplete("search", "");

    //Brand input validation on focus change
    //**************************************        
    var match = $.inArray(brandTextInput, brands);
    if (match == -1 || match == 0) {
        $("#brand-error").html("<span class='brand-error'></span>");
    }
    else {
        $("#brand-error").html("");
    }

    //Disabling the model textbox if brand text is empty or wrong
    if ($('#brand-error').children().is("span") || $("#other-brand").val().length == 0) {
        $("#other-model").prop("disabled", true);
    }
    else {
        $("#other-model").prop("disabled", false);
    }
    });

    //******* 'Proceed' button on-submit function *****
    //******* Validation for proper input *************
    //*************************************************
    $("#proceed").on('click', function () {
        if ($('#brand-error').children().is("span") || $("#other-brand").val().length == 0
                || $("#other-model").val().length == 0 || $('#ram option:selected').val().length == 0
                || $('#internal-memory option:selected').val().length == 0
                || $('#camera option:selected').val().length == 0
                || $('#age option:selected').val().length == 0) {
            
            if ($('#other-brand-error').children().is("span") || $("#other-brand").val().length == 0) {
                $('#other-brand').css('border-color', 'red');
                $('#other-model').css('border-color', 'red');
                setTimeout(function () { $("#other-brand").css('border-color', ''); }, 3000);
                setTimeout(function () { $("#other-model").css('border-color', ''); }, 3000);
            }
            if ($("#other-model").val().length == 0) {
                $('#other-model').css('border-color', 'red');
                setTimeout(function () { $("#other-model").css('border-color', ''); }, 3000);
            }
            if ($('#ram option:selected').val() == "") {
                $('#ram').css('border-color', 'red');
                setTimeout(function () { $("#ram").css('border-color', ''); }, 3000);
            }
            if ($('#internal-memory option:selected').val() == "") {
                $('#internal-memory').css('border-color', 'red');
                setTimeout(function () { $("#internal-memory").css('border-color', ''); }, 3000);
            }
            if ($('#camera option:selected').val() == "") {
                $('#camera').css('border-color', 'red');
                setTimeout(function () { $("#camera").css('border-color', ''); }, 3000);
            }
            if ($('#age option:selected').val() == "") {
                $('#age').css('border-color', 'red');
                setTimeout(function () { $("#age").css('border-color', ''); }, 3000);
            }
            return false;
        }
        else {
            $('#prokart-loading-other').show();
            var data = {
                brand:              $("#other-brand").val(),
                model:              $("#other-model").val(),
                ram:                $('#ram option:selected').text(),
                memory:             $('#internal-memory option:selected').text(),
                camera:             $('#camera option:selected').text(),
                releaseDate:        $('#age option:selected').text()
            };

            //***** Ajax call function to controller method to pass on the information
            //************************************************************************
            $.ajax({
                url: 'http://prokart.us-west-2.elasticbeanstalk.com/Sell/OtherDevices',
                data: data,
                type: 'POST',
                async: false,
                contenttype: "application/json",
                datatype: "json",
                success: function (result) {
                    $("#questionnaire-popup-body").load("http://prokart.us-west-2.elasticbeanstalk.com/Sell/Questionnaire");
                    $("#questionnaire-modal").show();
                    var questmodal = document.getElementById('questionnaire-modal');
                    var span = document.getElementsByClassName("qtscloseother")[0];
                    questmodal.style.display = "block";
                    $("#prokart-loading-other").hide();
                    span.onclick = function () {
                        questmodal.style.display = "none";
                    }
                    window.onclick = function (event) {
                        if (event.target == questmodal) {
                            questmodal.style.display = "none";
                        }
                    }
                },
                error: function (error, status, msg) {
                    $('#prokart-loading-other').hide();
                    alert("Error");
                }
            });
        }
    });
})