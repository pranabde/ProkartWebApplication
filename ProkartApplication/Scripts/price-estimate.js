//======================================================================================
//************** price-estimate.js gives user a price quote based on his device ********
//************** condition input at previous page, device specification from    ********
//************** gsmarena.com & current market price from datawave.in           ********
//                                                                              ********
//======================================================================================
//****************************** Author: Pranab Kumar De *******************************
//======================================================================================

$(document).ready(function () {
    //Getting values from hidden field, which is controllr session variable
    var fromController = null;
    var fromControllerJson = $("#from-controller-questionnaire").val();
    fromController = JSON.parse(fromControllerJson);
    $("#prokart-loading-price").hide();
    //Initializing local variables
    var brand;
    var model;
    var ram;
    var memory;
    var camera;
    var releaseDate;
    var price;
    var batteryCondition, screenQuality, chargingPortCondition, cameraCondition, heatingProblem, softwareProblem;
    var pictureName = null;
    
    brand = fromController.Brand;
    model = fromController.Model;
    ram = fromController.Ram;
    memory = fromController.Memory;
    camera = fromController.Camera;
    releaseDate = fromController.ReleaseDate;
    price = fromController.Price;
    batteryCondition = fromController.BatteryCondition;
    screenQuality = fromController.ScreenQuality;
    chargingPortCondition = fromController.ChargingPortCondition;
    cameraCondition = fromController.CameraCondition;
    heatingProblem = fromController.HeatingProblem;
    softwareProblem = fromController.SoftwareProblem;
    pictureName = fromController.PictureName;

    $('#priceValue').html(price);
          
    //***** On 'Proceed' button click, Ajax call to controller method 
    //***** to pass on the information ******************************
    //***************************************************************
    $("#price-estimate-proceed").on('click', function () {
        $("#prokart-loading-price").show();
        var data = {
            brand: brand,
            model: model,
            ram: ram,
            memory: memory,
            camera: camera,
            releaseDate: releaseDate,
            price: price,
            batteryCondition: batteryCondition,
            screenQuality: screenQuality,
            chargingPortCondition: chargingPortCondition,
            cameraCondition: cameraCondition,
            heatingProblem: heatingProblem,
            softwareProblem: softwareProblem,
            pictureName: pictureName
        };
        $.ajax({
            url: 'http://prokart.us-west-2.elasticbeanstalk.com/Sell/PriceEstimate',
            data: data,
            type: 'POST',
            async: false,
            contenttype: "application/json",
            success: function (result) {
                $("#checkout-popup-body").load("http://prokart.us-west-2.elasticbeanstalk.com/Sell/CheckOut");
                $("#checkout-modal").show();
                var checkoutmodal = document.getElementById('checkout-modal');
                var pricemodal = document.getElementById('price-modal');
                var questmodal = document.getElementById('questionnaire-modal');
                var specmodal = document.getElementById('specification-modal');
                var span = document.getElementsByClassName("checkoutclose")[0];
                pricemodal.style.display = "block";
                $("#prokart-loading-price").hide();
                span.onclick = function () {
                    checkoutmodal.style.display = "none";
                    pricemodal.style.display = "none";
                    questmodal.style.display = "none";
                    specmodal.style.display = "none";
                }
                window.onclick = function (event) {
                    if (event.target == checkoutmodal) {
                        checkoutmodal.style.display = "none";
                        pricemodal.style.display = "none";
                        questmodal.style.display = "none";
                        specmodal.style.display = "none";
                    }
                }
            },
            error: function (error, status, msg) {
                $("#prokart-loading-price").hide();
                alert("Error while loading price");
            }
        });
    });

    
});