$( document ).ready(function() {
	$("#prokart_loading").hide();
	/*$('.form').find('input, textarea').on('keyup blur focus', function (e) {

		var $this = $(this),
		label = $this.prev('label');

		if (e.type === 'keyup') {
			if ($this.val() === '') {
				label.removeClass('active highlight');
			} else {
				label.addClass('active highlight');
			}
		} else if (e.type === 'blur') {
			if( $this.val() === '' ) {
				label.removeClass('active highlight'); 
			} else {
				label.removeClass('highlight');   
			}   
		} else if (e.type === 'focus') {

			if( $this.val() === '' ) {
				label.removeClass('highlight'); 
			} 
			else if( $this.val() !== '' ) {
				label.addClass('highlight');
			}
		}

	});*/

	$('.tab a').on('click', function (e) {

		e.preventDefault();

		$(this).parent().addClass('active');
		$(this).parent().siblings().removeClass('active');

		target = $(this).attr('href');

		$('.tab-content > div').not(target).hide();

		$(target).fadeIn(600);

	});

	$("#prokart_firstname").on('input',function (e) {
		var firstname = $("#prokart_firstname").val();
		e.preventDefault();
		if(firstname){
			//$("#error_prokart_firstname").html("");
		}else{
			notify('error','First Name is required','/Resource/image/notification_error.png','2000','sound2');
			//$("#error_prokart_firstname").html("<span class='alert-box error'>First Name is required"+'</span>');
		}
	});

	$("#prokart_lastname").on('input',function (e) {
		var lastname = $("#prokart_lastname").val();
		e.preventDefault();
		if(lastname){
			//$("#error_prokart_lastname").html("");
		}else{
			notify('error','Last Name is required','/Resource/image/notification_error.png','2000','sound2');
			//$("#error_prokart_lastname").html("<span class='alert-box error'>Last Name is required"+'</span>');
		}
	});

	$("#prokart_email").on('input',function (e) {
		var email = $("#prokart_email").val();
		var EMAIL_REGEX = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		e.preventDefault();
		if(email){
		}else{
			notify('error','Email is required','/Resource/image/notification_error.png','2000','sound2');
			//$("#error_prokart_email").html("<span class='alert-box error'>Email is Required"+'</span>');
		}
	});

	$("#prokart_email").change(function(e){
		var emailValue = $("#prokart_email").val();
		var EMAIL_REGEX = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		e.preventDefault();
		if(EMAIL_REGEX.test(emailValue)){
			$.ajax({
				url : 'http://localhost:7080/prokart/emailalreadyexists?email='+emailValue,
				contentType : "application/json",
				type : "POST",
				success : function(response,status) {
					if(response.success_type == 'success'){
						notify('success',response.success_description,'/Resource/image/notification_success.png','2000','sound1');
						//$("#error_prokart_email").html("<span class='alert-box success'>"+response.success_description+'</span>');
					}
					if(response.error_type == 'error'){
						notify('error',response.error_description,'/Resource/image/notification_error.png','2000','sound2');
						//$("#error_prokart_email").html("<span class='alert-box error'>"+response.error_description+'</span>');
					}
				},
				error : function(errorresponse) {
					notify('error',errorresponse,'/Resource/image/notification_error.png','2000','sound2');
					//alert(errorresponse);
				}
			});
		}else{
			notify('error','Email is invalid!!!','/Resource/image/notification_error.png','2000','sound2');
		}
	});

	$("#prokart_mobile").on('input',function (e) {
		var mobile = $("#prokart_mobile").val();
		var MOB_REGEX = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
		e.preventDefault();
		if(mobile){
		}else{
			notify('error','Mobile number is required','/Resource/image/notification_error.png','2000','sound2');
			//$("#error_prokart_mobile").html("<span class='alert-box error'>Mobile Number is Required"+'</span>');
		}
	});

	$("#prokart_mobile").on('change',function (e) {
		var mobile = $("#prokart_mobile").val();
		var MOB_REGEX = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
		e.preventDefault();
		if(mobile){
			if(MOB_REGEX.test(mobile)){
				//$("#error_prokart_mobile").html("");
			}else{
				notify('error','Mobile number is invalid','/Resource/image/notification_error.png','2000','sound2');
				//$("#error_prokart_mobile").html("<span class='alert-box error'>Mobile Number is Invalid!!"+'</span>');
			}
		}
	});

	$("#prokart_password").on('input',function (e) {
		var password = $("#prokart_password").val();
		var pwdLength = password.length;
		e.preventDefault();
		if(password){
			//$("#error_prokart_password").html("");
		}else{
			notify('error','Password is required','/Resource/image/notification_error.png','2000','sound2');
			//$("#error_prokart_password").html("<span class='alert-box error'>Password is required"+'</span>');
		}
	});

	$("#prokart_password").on('change',function (e) {
		var password = $("#prokart_password").val();
		var pwdLength = password.length;
		e.preventDefault();
		if(pwdLength >0 && pwdLength < 5){
			notify('error','Password should be minimum 5 characters','/Resource/image/notification_error.png','2000','sound2');
			//$("#error_prokart_password").html("<span class='alert-box error'>Password should be minimum 5 characters"+'</span>');
		}
	});

	$("#prokart_signup").click(function(e) {
		//alert("clicked");
		e.preventDefault();
		$("#prokart_loading").show();
		//$(".prokart_signup_form").validate({
		//submitHandler: function(form) {
		var formData = {
				'firstname' : $("#prokart_firstname").val(),
				'lastname' : $("#prokart_lastname").val(),
				'email' : $("#prokart_email").val(),
				'mobile' : $("#prokart_mobile").val(),
				'password' : $("#prokart_password").val(),
		};
		$.ajax({
			url : 'http://localhost:7080/prokart/signup',
			contentType : "application/json",
			dataType : "json",
			type : "POST",
			data : JSON.stringify(formData),
			success : function(response,status) {
				$("#prokart_loading").hide();
				if(response.success_type == 'success'){
					notify('success',response.success_description,'/Resource/image/notification_success.png','5000','sound1');
					//alert(response.success_description);	
				}
				if(response.error_type == 'error'){
					notify('error',response.error_description,'/Resource/image/notification_success.png','5000','sound2');
					//alert(response.error_description);	
				}
				if(response.firstname){
					notify('error',response.firstname,'/Resource/image/notification_error.png','5000','sound2');
					//$("#error_prokart_firstname").html("<span class='alert-box error'>First Name is required"+'</span>');
				}
				if(response.lastname){
					notify('error',response.lastname,'/Resource/image/notification_error.png','5000','sound2');
					//$("#error_prokart_lastname").html("<span class='alert-box error'>Last Name is required"+'</span>');
				}
				if(response.email){
					notify('error',response.email,'/Resource/image/notification_error.png','5000','sound2');
					//$("#error_prokart_email").html("<span class='alert-box error'>"+response.email+'</span>');
				}
				if(response.mobile){
					notify('error',response.mobile,'/Resource/image/notification_error.png','5000','sound2');
					//$("#error_prokart_mobile").html("<span class='alert-box error'>"+response.mobile+'</span>');
				}
				if(response.password){
					notify('error',response.password,'/Resource/image/notification_error.png','5000','sound2');
					//$("#error_prokart_password").html("<span class='alert-box error'>Password should be minimum 5 characters"+'</span>');
				}
			},
			error : function(response,status,condition) {
				$("#prokart_loading").hide();
				notify('error','Error while signing up','/Resource/image/notification_error.png','5000','sound2');
				//alert("Error while signing up");
			}
		});
		//	}
		//});
	});
});

function notify(notificationType,message,imagePath,timeDelay,soundTrack){
	Lobibox.notify(
			notificationType, 				// Available types 'warning', 'info', 'success', 'error'
			{
				title: null,                // Title of notification. Do not include it for default title or set custom string. Set this false to disable title
				size: 'mini',             			// normal, mini, large
				soundPath: '/Resource/sounds/',   // The folder path where sounds are located
				soundExt: '.ogg',           // Default extension for all sounds
				showClass: 'fadeInDown',        // Show animation class.
				hideClass: 'fadeUpDown',       // Hide animation class.
				icon: true,                 // Icon of notification. Leave as is for default icon or set custom string
				msg: message,// Message of notification
				img: imagePath,                  // Image source string
				closable: true,             // Make notifications closable
				delay: timeDelay,           // Hide notification after this time (in miliseconds)
				delayIndicator: true,       // Show timer indicator
				closeOnClick: true,         // Close notifications by clicking on them
				width: 300,                 // Width of notification box
				sound: soundTrack,                // Sound of notification. Set this false to disable sound. Leave as is for default sound or set custom soud path
				position: "top right",    // Place to show notification. Available options: "top left", "top right", "bottom left", "bottom right"
				iconSource: "bootstrap",     // "bootstrap" or "fontAwesome" the library which will be used for icons
				rounded: false,             // Whether to make notification corners rounded
				messageHeight: 60 
			});
}

