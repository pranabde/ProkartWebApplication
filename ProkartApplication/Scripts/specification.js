﻿//======================================================================================
//************** specification.js populates a specific device information in table *****
//                                                                                 *****
//======================================================================================
//****************************** Author: Pranab Kumar De *******************************
//======================================================================================

$(document).ready(function () {
    //Getting values from hidden field, which is controllr session variable
    var fromController = null;
    var fromControllerJson = $("#from-controller-search").val();
    fromController = JSON.parse(fromControllerJson);

    $("#prokart-loading-specification").hide();
    var modelId; 
    var picturelink;

    $("#brand-name").val(fromController.Brand);
    $("#model-name").text(fromController.Model);
    modelId = fromController.ModelId;
    picturelink = fromController.PictureLink;
    //**** Ajax call to get device specification from gsmarena.com
    //**** for a specific model id *******************************
    //************************************************************
    $.ajax({
        url: 'http://www.gsmarena.com/phone-widget.php3?idPhone=' + modelId,
        type: 'GET',
        async: false,
        datatype: "html",
        success: function (result) {
            $('#specification_table').append(result);
        },
        error: function (error, status, msg) {
            alert("Error fetching specifications from Internet");
        }
    });

    //**** Calling gsmarena.com to get device picture for a 
    //**** specific picture link and showing in table *****
    //*****************************************************
    var picUrl = 'http://cdn2.gsmarena.com/vv/bigpic/' + picturelink;
    $("#picture").attr('src', picUrl);

    //***** 'Proceed' button click
    //****************************
    $("#proceed_specification").on('click', function () {
        $("#prokart-loading-specification").show();
        var tableData = [];
        for (var i = 0; i < 10; i++) {
            var tRows = document.getElementsByClassName("nfo");
            tableData.push(tRows[i].textContent);
        }
        var data = {
            brand: $("#brand-name").val(),
            model: $("#model-name").text(),
            releaseDateInput: tableData[0],
            memoryInput: tableData[5],
            cameraInput: tableData[6]           
        };

        //***** Ajax call to controller method to pass on the information
        //***************************************************************
        $.ajax({
            url: 'http://prokart.us-west-2.elasticbeanstalk.com/Sell/Specification',
            data: data,
            type: 'POST',
            async: false,
            contenttype: "application/json",
            datatype:"json",
            success: function (result) {
                $("#questionnaire-popup-body").load("http://prokart.us-west-2.elasticbeanstalk.com/Sell/Questionnaire");
                $("#questionnaire-modal").show();
                var questmodal = document.getElementById('questionnaire-modal');
                var specmodal = document.getElementById('specification-modal');
                var span = document.getElementsByClassName("qtsclose")[0];                
                questmodal.style.display = "block";                
                $("#prokart-loading-specification").hide();
                span.onclick = function () {
                    questmodal.style.display = "none";
                    specmodal.style.display = "none";
                }
                window.onclick = function (event) {
                    if (event.target == questmodal) {
                        questmodal.style.display = "none";
                        specmodal.style.display = "none";
                    }
                }
            },
            error: function (error, status, msg) {
                alert("Error");
            }
        });
    });
});
