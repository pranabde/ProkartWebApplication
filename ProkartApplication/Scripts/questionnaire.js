//======================================================================================
//************** questionnaire.js asks user about device condition to evaluate price ***
//                                                                                   ***
//======================================================================================
//****************************** Author: Pranab Kumar De *******************************
//======================================================================================

$(document).ready(function () {
    //Getting values from hidden field, which is controllr session variable
    var fromController = null;
    var fromControllerJson = $("#from-controller-specification").val();
    fromController = JSON.parse(fromControllerJson);

    $("#prokart-loading-questionnaire").hide();
    $("input").prop('checked', false);
    var brand;
    var model;
    var releaseDate;
    var pictureName = "";
    var sessionId = "";
    var priceList = [];
    var initialPrice = 0;
    var price;
    var images = [];
    
    $("#brand").val(fromController.Brand);
    $("#model").val(fromController.Model);
    releaseDate = fromController.ReleaseDate;
    $("#ram-value").val(fromController.Ram);
    $("#memory-value").val(fromController.Memory);
    $("#camera-value").val(fromController.Camera);
    var battery = 0;
    var screen = 0;
    var charging = 0;
    var camera = 0;
    var heating = 0;
    var software = 0;

    //checking if the radio selections are empty
    //******************************************
    $("input").on('click', function (event) {
        var par = $(event.target).parent().attr('id');
        if (par == "battery") {
            battery = $(event.target).attr('value');            
        }
        else if (par == "screen") {
            screen = $(event.target).attr('value');
        }
        else if (par == "cameravalue") {
            camera = $(event.target).attr('value');
        }
        else if (par == "charging") {
            charging = $(event.target).attr('value');
        }        
        else if (par == "heating") {
            heating = $(event.target).attr('value');
        }
        else if (par == "software") {
            software = $(event.target).attr('value');
        }
    });

    //************* Picture upload section **************
    //* Maximum number of images can be uploaded is 2 *//
    //***************************************************
    $("#devicePicture").change(function () {
        var isAnyFileOutOfSize = false;
        if (parseInt($("#devicePicture").get(0).files.length) > 2) {
            $("#devicePicture").replaceWith($("#devicePicture").val('').clone(true));
            var countalertmodal = document.getElementById("count-alert-modal");
            countalertmodal.style.display = "block";
            setTimeout(function () { countalertmodal.style.display = "none"; }, 2000);           
        }
        else {
            var data = new FormData();
            var files = $("#devicePicture").get(0).files;
            var maxSize = $("#devicePicture").data("max-size");
            var firstImageSize = $("#devicePicture").get(0).files[0].size;
            if (files.length > 1)
                var secondImageSize = $("#devicePicture").get(0).files[1].size;
            if (files.length > 0) {
                data.append("FirstImage", files[0]);
                if (firstImageSize > maxSize)
                    isAnyFileOutOfSize = true;
            }
            if (files.length > 1) {
                data.append("FirstImage", files[0]);
                data.append("SecondImage", files[1]);
                if (firstImageSize > maxSize || secondImageSize > maxSize)
                    isAnyFileOutOfSize = true;
            }
            if (isAnyFileOutOfSize == false) {
                $("#proceed-questionnaire").prop('disabled', true);
                $("#prokart-loading-questionnaire").show();
                $.ajax({
                    url: 'http://prokartwebservice.us-west-2.elasticbeanstalk.com/ProkartWebServices.svc/WebRequest/GetDevicePic',
                    type: "POST",
                    processData: false,
                    contentType: false,
                    data: data,
                    success: function (response) {
                        $("#proceed-questionnaire").prop('disabled', false);
                        $("#prokart-loading-questionnaire").hide();
                        $("#devicePicture").prop('disabled', true);
                        if (response == "Incorrect File Format.") {
                            $("#devicePicture").replaceWith($("#devicePicture").val('').clone(true));
                            $("#devicePicture").prop('disabled', false);
                            var erroralertmodal = document.getElementById("error-alert-modal");
                            erroralertmodal.style.display = "block";
                            $("#error-msg").text("Incorrect File Format.");
                            setTimeout(function () { erroralertmodal.style.display = "none"; }, 2000);
                        }
                        if (response == "File Upload Failure.") {
                            $("#devicePicture").replaceWith($("#devicePicture").val('').clone(true));
                            $("#devicePicture").prop('disabled', false);
                            var erroralertmodal = document.getElementById("error-alert-modal");
                            erroralertmodal.style.display = "block";
                            $("#error-msg").text("Failed To Upload File.");
                            setTimeout(function () { erroralertmodal.style.display = "none"; }, 2000);
                        }
                        else {
                            var successalertmodal = document.getElementById("success-alert-modal");
                            successalertmodal.style.display = "block";
                            setTimeout(function () { successalertmodal.style.display = "none"; }, 2000);
                            response = JSON.parse(response);
                            pictureName = response.PictureName;
                            sessionId = response.SessionId;
                        }
                    },
                    error: function (er) {
                        $("#prokart-loading-questionnaire").hide();
                        $("#devicePicture").prop('disabled', false);
                        var erroralertmodal = document.getElementById("error-alert-modal");
                        erroralertmodal.style.display = "block";
                        setTimeout(function () { erroralertmodal.style.display = "none"; }, 2000);
                    }
                });
            }
            else {
                $("#devicePicture").replaceWith($("#devicePicture").val('').clone(true));
                var sizealertmodal = document.getElementById("size-alert-modal");
                sizealertmodal.style.display = "block";
                setTimeout(function () { sizealertmodal.style.display = "none"; }, 2000);
            }
        }
    });
    
    //***** 'Proceed' Button click function *****
    //***** Checking if any field is empty ******
    //*******************************************
    $("#proceed-questionnaire").on('click', function () {
        if (battery == 0 || screen == 0 || charging == 0 || camera == 0 || heating == 0 || software == 0) {
            if (battery == 0) {
                $('#battery').parent().css('border-color', 'red');
                setTimeout(function () { $("#battery").parent().css('border-color', ''); }, 3000);
            }
            if (screen == 0) {
                $('#screen').parent().css('border-color', 'red');
                setTimeout(function () { $("#screen").parent().css('border-color', ''); }, 3000);
            }
            if (camera == 0) {
                $('#cameravalue').parent().css('border-color', 'red');
                setTimeout(function () { $("#cameravalue").parent().css('border-color', ''); }, 3000);
            }
            if (charging == 0) {
                $('#charging').parent().css('border-color', 'red');
                setTimeout(function () { $("#charging").parent().css('border-color', ''); }, 3000);
            }            
            if (heating == 0) {
                $('#heating').parent().css('border-color', 'red');
                setTimeout(function () { $("#heating").parent().css('border-color', ''); }, 3000);
            }
            if (software == 0) {
                $('#software').parent().css('border-color', 'red');
                setTimeout(function () { $("#software").parent().css('border-color', ''); }, 3000);
            }
            return false;
        }
        else {
            $('#prokart-loading-questionnaire').show();
            setTimeout(ajaxCall, 2500);
        }
    });

    //Ajax call to datawave.in for current market price
    //*************************************************
    function ajaxCall() {
        brand = $("#brand").val();
        model = $("#model").val();
        model = model.replace(/\s+/g, '+').toLowerCase();

        $.ajax({
            url: 'http://api.dataweave.in/v1/price_intelligence/findProduct/?api_key=b20a79e582ee4953ceccf41ac28aa08d&product=' + brand + "+" + model,
            type: 'GET',
            async: false,
            datatype: "json",
            statusCode: {
            },
            success: function (result) {
                for (var i = 0; i < result.data.length; i++) {
                    if ((result.data[i].category_tagged == '"tablets"' || result.data[i].category_tagged == '"mobile"')) {
                        priceList.push(result.data[i].available_price);
                    }
                }
            },
            error: function (error, status, msg) {
                alert("Error while loading price");
            },
        });
        if (priceList.length > 0) {
            for (var i = 0; i < priceList.length; i++) {
                initialPrice += parseInt(priceList[i], 10);
            }
            price = parseInt((initialPrice / priceList.length), 10);
        }
        if (priceList.length == 0) {
            price = 0;
        }

        var data = {
            brand: $("#brand").val(),
            model: $("#model").val(),
            releaseDate: releaseDate,
            ram: $("#ram-value").val(),
            memory: $("#memory-value").val(),
            camera: $("#camera-value").val(),
            batteryCondition: battery,
            screenQuality: screen,
            chargingPortCondition: charging,
            cameraCondition: camera,
            heatingProblem: heating,
            softwareProblem: software,
            actualPrice: price,
            pictureName: pictureName,
            sessionId: sessionId
        };

        //***** Ajax call to controller method to pass on the information
        //***************************************************************
        $.ajax({
            url: 'http://prokart.us-west-2.elasticbeanstalk.com/Sell/Questionnaire',
            data: data,
            type: 'POST',
            async: false,
            contenttype: "application/json",
            datatype: "json",
            success: function (result) {
                $("#price-popup-body").load("http://prokart.us-west-2.elasticbeanstalk.com/Sell/PriceEstimate");
                $("#price-modal").show();                
                var pricemodal = document.getElementById('price-modal');
                var questmodal = document.getElementById('questionnaire-modal');
                var specmodal = document.getElementById('specification-modal');
                var span = document.getElementsByClassName("priceclose")[0];
                pricemodal.style.display = "block";
                $("#prokart-loading-questionnaire").hide();
                span.onclick = function () {
                    pricemodal.style.display = "none";
                    questmodal.style.display = "none";
                    specmodal.style.display = "none";                   
                }
                window.onclick = function (event) {
                    if (event.target == pricemodal) {
                        pricemodal.style.display = "none";
                        questmodal.style.display = "none";
                        specmodal.style.display = "none";
                    }
                }
            },
            error: function (error, status, msg) {
                alert("Error");
            }
        });
    }
});

