$( document ).ready(function() {
	
	/*$('.tab a').on('click', function (e) {

		e.preventDefault();

		$(this).parent().addClass('active');
		$(this).parent().siblings().removeClass('active');

		target = $(this).attr('href');

		$('.tab-content > div').not(target).hide();

		$(target).fadeIn(600);

	});*/
	
	$("#prokart_loading_login").hide();
	$("#forgot_pwd").hide();
	$.cookie.join = true;
	$.cookie.raw  = true;
	$("#prokart_login_email").on('input',function (e) {
		var email = $("#prokart_login_email").val();
		var EMAIL_REGEX = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		e.preventDefault();
		if(email){
		}else{
			notify('error','Email is required','/Resource/image/notification_error.png','2000','sound2');
			//$("#error_prokart_login_email").html("<span class='alert-box error'>Email is Required"+'</span>');
		}
	});

	$("#prokart_login_email").on('change',function (e) {
		var email = $("#prokart_login_email").val();
		var EMAIL_REGEX = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		e.preventDefault();
		if(EMAIL_REGEX.test(email)){
			//$("#error_prokart_login_email").html("");
		}else{
			notify('error','Email is invalid!!','/Resource/image/notification_error.png','2000','sound2');
			//$("#error_prokart_login_email").html("<span class='alert-box error'>Email is Invalid!!"+'</span>');
		}

	});


	$("#prokart_login_password").on('input',function (e) {
		var password = $("#prokart_login_password").val();
		var pwdLength = password.length;
		e.preventDefault();
		if(password){
			//$("#error_prokart_login_password").html("");
		}else{
			notify('error','Password is required','/Resource/image/notification_error.png','2000','sound2');
			//$("#error_prokart_login_password").html("<span class='alert-box error'>Password is required"+'</span>');
		}
	});

	$("#prokart_login").click(function(e) {
		e.preventDefault();
		$("#prokart_loading_login").show();
		var date = new Date();
		var formData = {
				'email' : $("#prokart_login_email").val(),
				'password' : $("#prokart_login_password").val(),
		};
		$.ajax({
			url : 'http://localhost:7080/prokart/login',
			contentType : "application/json",
			dataType : "json",
			type : "POST",
			data : JSON.stringify(formData),
			async: false,
			success : function(response,status) {
				$("#prokart_loading_login").hide();

				if(response.userid){
					//alert("Successfully logged in!!");
					date.setTime(date.getTime() + (59 * 60 * 1000));
					notify('success','Successfully logged in!!','/Resource/image/notification_success.png','5000','sound1');
					$.cookie("userid",response.userid,{ expires: date });
					$.cookie("generated_token",response.generated_token,{ expires: date });
				}
				if(response.error_type == 'error'){
					notify('error',response.error_description,'/Resource/image/notification_error.png','2000','sound2');
				}
				if(response.email){
					notify('error',response.email,'/Resource/image/notification_error.png','2000','sound2');
					//$("#error_prokart_login_email").html("<span class='alert-box error'>"+response.email+'</span>');
				}
				if(response.password){
					notify('error',response.password,'/Resource/image/notification_error.png','2000','sound2');
					//$("#error_prokart_login_password").html("<span class='alert-box error'>"+response.password+'</span>');
				}
			},
			error : function(response,status,condition) {
				$("#prokart_loading_login").hide();
				notify('error','Error while logging in','/Resource/image/notification_error.png','2000','sound2');
			}
		});
	});

	$("#prokart_login_fb").click(function(e) {
		window.location.replace("http://localhost:7081/prokart/login/fb");
	});

	$("#prokart_login_google").click(function(e) {
		window.location.replace("http://localhost:7081/prokart/login/google");
	});
	
	decodeFbUri();
	decodeGoogleUri();

});

function notify(notificationType,message,imagePath,timeDelay,soundTrack){
	Lobibox.notify(
			notificationType, 				// Available types 'warning', 'info', 'success', 'error'
			{
				title: null,                // Title of notification. Do not include it for default title or set custom string. Set this false to disable title
				size: 'mini',             			// normal, mini, large
				soundPath: '/Resource/sounds/',   // The folder path where sounds are located
				soundExt: '.ogg',           // Default extension for all sounds
				showClass: 'fadeInDown',        // Show animation class.
				hideClass: 'fadeUpDown',       // Hide animation class.
				icon: true,                 // Icon of notification. Leave as is for default icon or set custom string
				msg: message,// Message of notification
				img: imagePath,                  // Image source string
				closable: true,             // Make notifications closable
				delay: timeDelay,           // Hide notification after this time (in miliseconds)
				delayIndicator: true,       // Show timer indicator
				closeOnClick: true,         // Close notifications by clicking on them
				width: 300,                 // Width of notification box
				sound: soundTrack,                // Sound of notification. Set this false to disable sound. Leave as is for default sound or set custom soud path
				position: "top right",    // Place to show notification. Available options: "top left", "top right", "bottom left", "bottom right"
				iconSource: "bootstrap",     // "bootstrap" or "fontAwesome" the library which will be used for icons
				rounded: false,             // Whether to make notification corners rounded
				messageHeight: 60 
			});
}
function decodeFbUri(){
	var sPageURL = decodeURIComponent(window.location.search.substring(1));
	var sParameterName = sPageURL.split('=');
	if (sParameterName[0] === 'code') {
		$("#prokart_loading_login").show();
		var code = sParameterName[1];
		$.ajax({
			url : 'http://localhost:7080/prokart/fblogin',
			contentType : "application/json",
			dataType : "json",
			type : "POST",
			data : code,
			async: false,
			success : function(response,status) {
				//alert("success");
				$("#prokart_loading_login").hide();
				if(response.error_type == 'fb_code_error'){
					notify('error',response.error_description,'/Resource/image/notification_error.png','2000','sound2');
					//alert(response.error_description);	
				}
				if(response.error_type == 'fb_token_error'){
					notify('error',response.error_description,'/Resource/image/notification_error.png','2000','sound2');
					//alert(response.error_description);	
				}
				if(response.error_type == 'fb_profile_error'){
					notify('error',response.error_description,'/Resource/image/notification_error.png','2000','sound2');
					//alert(response.error_description);	
				}
				if(response.error_type == 'fb_profile_persist_error'){
				    notify('error', response.error_description, '/Resource/image/notification_error.png', '2000', 'sound2');
					//alert(response.error_description);	
				}
				if(response.userid){
				    notify('success', 'Welcome to Prokart', '/Resource/image/notification_success.png', '2000', 'sound1');
					//alert("Welcome to Prokart");	
				}
			},
			error : function(response,status,condition) {
				$("#prokart_loading_login").hide();
				notify('error', 'Error while log in', '/Resource/image/notification_error.png', '2000', 'sound2');
			}
		});
	}
}

function decodeGoogleUri(){
	var sPageURL = window.location.href.slice(window.location.href.indexOf('#') + 1).split('=');//decodeURIComponent(window.location.search("access_token"));
	var accessToken = sPageURL[1].split('&')[0];

	if (accessToken.length > 1) {
		$("#prokart_loading_login").show();
		$.ajax({
			url : 'http://localhost:7080/prokart/googleprofile',
			contentType : "application/json",
			dataType : "json",
			type : "POST",
			data : accessToken,
			success : function(response,status) {
				$("#prokart_loading_login").hide();
				if(response.error_type == 'google_access_token_error'){
				    notify('error', response.error_description, '/Resource/image/notification_error.png', '2000', 'sound2');
				}
				if(response.error_type == 'google_profile_error'){
				    notify('error', response.error_description, '/Resource/image/notification_error.png', '2000', 'sound2');
				}
				if(response.error_type == 'google_email_error'){
				    notify('error', response.error_description, '/Resource/image/notification_error.png', '2000', 'sound2');
				}
				if(response.error_type == 'google_profile_persist_error'){
				    notify('error', response.error_description, '/Resource/image/notification_error.png', '2000', 'sound2');
				}
				if(response.userid){
				    notify('success', 'Welcome to Prokart', '/Resource/image/notification_success.png', '2000', 'sound1');
				}
			},
			error : function(response,status,condition) {
				$("#prokart_loading_login").hide();
				notify('error', 'Error while log in', '/Resource/image/notification_error.png', '2000', 'sound2');
			}
		});
	}
}